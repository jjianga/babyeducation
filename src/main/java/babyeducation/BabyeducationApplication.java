package babyeducation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabyeducationApplication {

    public static void main(String[] args) {
        SpringApplication.run(BabyeducationApplication.class, args);
    }

}
