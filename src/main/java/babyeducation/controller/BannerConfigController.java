package babyeducation.controller;

import babyeducation.model.BannerConfig;
import babyeducation.model.Manager;
import babyeducation.service.BannerConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/banner")
@Api(value = "/api/banner", description = "banner本相关接口")
public class BannerConfigController extends BaseController {

    @Autowired
    BannerConfigService bannerConfigService;


    @PostMapping(value = "/addBanner")
    @ApiOperation(value = "/addBanner")
    public Map<String, Object> addBanner(HttpSession session, BannerConfig bannerConfig) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            bannerConfigService.insertSelective(bannerConfig);
            return successResult("添加成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @DeleteMapping(value = "/deleteBanner")
    @ApiOperation(value = "/deleteBanner", notes = "删除banner")
    public Map<String, Object> deleteBanner(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            bannerConfigService.deleteByPrimaryKey(id);
            return successResult("删除成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PutMapping(value = "/updateBanner")
    @ApiOperation(value = "/updateBanner", notes = "修改banner")
    public Map<String, Object> updateBanner(HttpSession session, BannerConfig bannerConfig) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            bannerConfigService.updateByPrimaryKeySelective(bannerConfig);
            return successResult("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }

    }

    @PostMapping(value = "/bannerInfo")
    @ApiOperation(value = "/bannerInfo", notes = "/主推位广告")
    public Map<String, Object> bannerInfo() {
        try {
            List<BannerConfig> bannerConfigList = bannerConfigService.selectBannerConfig();
            return successResult("查询成功", bannerConfigList);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult("系统错误", null);
        }
    }
}
