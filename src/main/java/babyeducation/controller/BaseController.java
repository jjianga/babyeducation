package babyeducation.controller;

import babyeducation.dao.ConfigMapMapper;
import babyeducation.model.ConfigMap;
import babyeducation.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * BaseController
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public class BaseController {

    private static final String FORMAT = "yyyy-MM-dd";


    @Autowired
    ConfigMapMapper configMapMapper;
    @Autowired
    RedisUtil redisUtil;

    @Value("${tokenFailed}")
    String tokenFailed;
    @Value("${loginSuccess}")
    String loginSuccess;
    @Value("${addSuccess}")
    String addSuccess;
    @Value("${addFailed}")
    String addFailed;
    @Value("${updateSuccess}")
    String updateSuccess;
    @Value("${updateFailed}")
    String updateFailed;
    @Value("${deleteSuccess}")
    String deleteSuccess;
    @Value("${deleteFailed}")
    String deleteFailed;
    @Value("${loginOut}")
    String loginOut;
    @Value("${tokenSuccess}")
    String tokenSuccess;
    @Value("${RESFailed}")
    String RESFailed;
//    @Autowired
//    JedisPool jedisPool;

    @Value("校验码过期请重新登陆")
    String tokensFailed;

    final Logger logger = LoggerFactory.getLogger(BaseController.class);

    public Map<String, Object> successResult(Object... result) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("message", result[0]);
        map.put("data", result[1]);
        return map;
    }

    public Map<String, Object> failedResult(Object... result) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", result[0]);
        map.put("message", result[1]);
        map.put("data", result[2]);
        return map;
    }

    // -1 非法用户  1正确用户 0用户登陆过期
    public int textToken(HttpServletRequest req, String mob) {
            return 1;
//        String header = req.getHeader("token_id");
//        String token = redisUtil.get(mob + "_token");
//        if (null == token) {
//            return -1;
//        }
//        if (token.equals(header)) {
//            return 1;
//        } else {
//            return 0;
//        }
    }

    public Date getEndTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
        long time = date.getTime();
        ConfigMap configMap = configMapMapper.selectConfigMapByKey("time");
        String dates = configMap.getValue();
        Integer day = Integer.valueOf(dates);
        Long date1 = time + (1000 * 60 * 60 * 24 * day);
        Date endTime = new Date(date1);
        return endTime;
    }

//    public void saveToken(String key, String value)  throws Exception {
//        String keyToken = key + "_token";
//

//        Jedis jedis = getJedis();
//        String keyToken = key + "_token";
//        boolean keyExist = redisUtil.exists(keyToken);
//        if (keyExist) {
//            redisUtil.(keyToken);
//        }
//        jedis.set(keyToken, value);
//        jedis.expire(keyToken, 60 * 60 * 24 * 5);

//
//    }

//    public synchronized Jedis getJedis()  {
//
//        Jedis jedis = null;
//        if (null == jedis) {
//            try {
//                jedis = jedisPool.getResource();
//            } catch (Exception e) {
//                e.printStackTrace();
//                returnResource(jedis);
//                returnBrokenResource(jedis);
//            }
//
//
//        }
//        return jedis;
//    }
//
//    public synchronized void returnResource(Jedis jedis) {
//        if (null != jedis) {
//            jedisPool.returnResource(jedis);
//        }
//    }
//
//    public synchronized void returnBrokenResource(Jedis jedis) {
//        if (null != jedis) {
//            jedisPool.returnBrokenResource(jedis);
//        }
//    }
}
