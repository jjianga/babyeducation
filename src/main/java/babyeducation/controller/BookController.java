package babyeducation.controller;

import babyeducation.model.*;
import babyeducation.service.*;
import babyeducation.utils.Utils;
import babyeducation.utils.WebFileUtils;
import babyeducation.utils.ZipUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/book")
@Api(value = "/api/book", description = "点读课本相关接口")
public class BookController extends BaseController {

    @Autowired
    BookService bookService;
    @Autowired
    UserService userService;
    @Autowired
    VideoService videoService;
    @Autowired
    UserVideoBookLogService userVideoBookLogService;


    @Autowired
    UserBookLogService userBookLogService;
    @Autowired
    UserRecordsService userRecordsService;

    @PostMapping(value = "/selectBook")
    @ApiOperation(value = "/selectBook")
    public Map<String, Object> selectBook(HttpServletRequest req, String mob, Integer gardenId, Integer pressId, Integer subjectId, Integer volume, Integer hasVideo, Integer hasBook, Integer pageNum, Integer pageSize) {
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            if (null != pageNum && null != pageSize) {
                PageHelper.startPage(pageNum, pageSize);
            }
            List<Book> bookList = null;
            try {
                bookList = bookService.selectBookForHomePage(null,
                        gardenId,
                        pressId,
                        subjectId,
                        volume, hasVideo,
                        hasBook);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return successResult("查询成功", new PageInfo<>(bookList));
        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }

    @PostMapping(value = "/selectBookVideoById")
    @ApiOperation(value = "/selectBookVideoById", notes = "根据ID查Video列表")
    public Map<String, Object> selectBookVideoById(HttpServletRequest req, String mob, Integer bookId) {

        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {

            try {
                List<User> userList = userService.userLogin(mob);
                User user = userList.get(0);
                bookService.addReadNumByBookId(bookId);
                userVideoBookLogService.updateCreateTimeById(user.getUserId(), bookId);
                List<Video> videoList = videoService.selectVideoByVbuId(bookId);
                return successResult("查询成功", videoList);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }
        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }


    }

    @PostMapping(value = "/selectBookById")
    @ApiOperation(value = "/selectBookById", notes = "根据书本ID查是否可以下载点读课本")
    public Map<String, Object> selectBookById(HttpServletRequest req, String mob, Integer bookId) {
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            try {
                List<User> userList = userService.userLogin(mob);
                User user = userList.get(0);
                int hasVip = user.getIsVip();
                Book bookList = bookService.selectBookByBookId(bookId);
                if (null == bookList) {
                    return failedResult(200, "没有此书本", bookId);
                }
                bookService.addReadNumByBookId(bookId);
                userBookLogService.updateCreateTimeById(user.getUserId(), bookId);
                Integer isFree = bookList.getIsFree();
                if (0 == hasVip) {
                    if (0 == isFree) {
                        Integer userId = user.getUserId();
                        List<UserRecords> userRecordsList = userRecordsService.isRecordsByUserId(userId, bookId);
                        if (0 == userRecordsList.size()) {
                            bookList.setCanRead(false);
                            bookList.setIsVip(hasVip);
                            return successResult("请购买或者充值VIP会员", bookList);
                        }
                    }
                }
                bookList.setIsVip(hasVip);
                bookList.setCanRead(true);
                return successResult("查询成功", bookList);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }

        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }

    @PostMapping(value = "/addBook")
    @ApiOperation(value = "/addBook", notes = "添加点读课本")
    public Map<String, Object> addBook(HttpSession session, Book book) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            bookService.insertSelective(book);
            return successResult("点读课本添加成功", null);

        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @DeleteMapping(value = "/deleteBook")
    @ApiOperation(value = "/deleteBook", notes = "删除点读课本")
    public Map<String, Object> deleteBook(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null == id) {
            return failedResult(200, "id不能为空", null);
        }
        try {
            bookService.deleteBook(id);
            return successResult("删除课本成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @PutMapping(value = "/updateBook")
    @ApiOperation(value = "/updateBook")
    public Map<String, Object> updateBook(HttpSession session, Book book) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        try {
            bookService.updateByPrimaryKeySelective(book);
            return successResult("修改课本成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @GetMapping(value = "/searchBookInfo")
    @ApiOperation(value = "/searchBookInfo", notes = "后端查询点读课本信息")
    public Map<String, Object> searchBookInfo(HttpSession session, String bookName,
                                              Integer gardenId,
                                              Integer pressId,
                                              Integer subjectId,
                                              Integer volume,
                                              Integer pageNum,
                                              Integer pageSize) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<Book> bookList = null;
        try {
            bookList = bookService.selectBookForHomePage(bookName, gardenId, pressId, subjectId, volume, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return successResult("查询成功", new PageInfo<>(bookList));
    }

    @PostMapping(value = "/selectBookToPageHome")
    @ApiOperation(value = "/selectBookToPageHome", notes = "查询前端top5")
    public Map<String, Object> selectBookToPageHome(HttpServletRequest req, String mob, Integer gardenId) {
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            PageHelper.startPage(1, 5);
            try {
                List<Book> bookList = bookService.selectBookForHomePage(null, gardenId, null, null, null, null, null);
                return successResult("查询成功", bookList);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "查询失败", null);
            }

        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }

    @PostMapping(value = "/selectBookByIdAndZip")
    @ApiOperation(value = "/selectBookByIdAndZip", notes = "根据书本ID查是否可以下载点读课本")
    public Map<String, Object> selectBookByIdAndZip(HttpServletRequest req, String src, String souseID) {
        File filePath = new File("D://xin", souseID);
        File fileSucceed = new File(filePath, "succeed");
        if (fileSucceed.exists()) {
            return successResult(200, "已经下载完成");
        } else {
            File fileLoad = new File(filePath, "load");
            if (fileLoad.exists()) {
                return successResult(403, "文件缓存中");
            } else {
                WebFileUtils.writer(fileLoad, "load");
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            WebFileUtils.downLoadFromUrl(src, filePath, souseID + ".zip", "");
                            ZipUtils.unzip(new File(filePath, souseID + ".zip"), filePath);
                            WebFileUtils.writer(fileSucceed, "succeed");
                        } catch (Exception e) {
                            fileLoad.delete();
                        }
                    }
                }.start();
            }
        }
        return successResult(402, "文件开始缓存");
    }

    @PostMapping(value = "/newSelectBookBase")
    @ApiOperation(value = "/newSelectBookBase", notes = "只查询人教版的接口")
    public Map<String, Object> newSelectBookBase(Integer gardenId, Integer subjectId,Integer pressId, Integer volume, Integer hasVideo, Integer hasBook) {


        List<Book> bookList = null;
        try {
            bookList = bookService.newSelectBookBase(
                    gardenId,pressId,
                    subjectId,volume,
                    hasVideo,
                    hasBook);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return successResult("查询成功",bookList );

    }
}
