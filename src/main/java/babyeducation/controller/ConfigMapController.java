package babyeducation.controller;

import babyeducation.model.ConfigMap;
import babyeducation.service.ConfigMapService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/configMap")
@Api(value = "/api/configMap", description = "点读课本相关接口")
public class ConfigMapController extends BaseController {

    @Autowired
    ConfigMapService configMapService;


//    @PostMapping("/selectConfigMap")
//    @ApiOperation(value = "/selectConfigMap", notes = "查询配置")
//    public Map<String, Object> selectConfigMap() {
//
//        try {
//            List<ConfigMap> configMapList = configMapService.selectConfigMap();
//            if (0 != configMapList.size()) {
//                Map<String, String> map = new HashMap<>();
//                for (int i = 0; i < configMapList.size(); i++) {
//
//                    String key = configMapList.get(i).getKey();
//                    String value = configMapList.get(i).getValue();
//                    map.put(key, value);
//                }
//                return successResult("查询成功", map);
//            }
//            return successResult("查询成功,没有设置map",null);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return failedResult(200, "系统错误", null);
//        }
//
//
//    }

}
