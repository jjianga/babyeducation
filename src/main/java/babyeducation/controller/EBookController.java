package babyeducation.controller;

import babyeducation.model.EBook;
import babyeducation.service.EBookService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/EBook")
@Api(value = "/api/EBook")
public class EBookController extends BaseController {

    @Autowired
    EBookService eBookService;


    @PostMapping(value = "/searchEBook")
    @ApiOperation(value = "/searchEBook", notes = "推广课本查询接口")
    public Map<String, Object> searchEBook(Integer gardenId, Integer subjectId, Integer eType, Integer pageNum, Integer pageSize) {


        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        try {
            List<EBook> eBookList = eBookService.selectEBookByParameter(gardenId, subjectId, eType);
            return successResult("查询成功", eBookList);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }


    }
}
