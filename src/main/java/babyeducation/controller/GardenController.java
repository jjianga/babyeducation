package babyeducation.controller;

import babyeducation.model.Garden;
import babyeducation.model.Manager;
import babyeducation.service.GardenService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/garden")
@Api(value = "/api/garden", description = "年级相关接口")
public class GardenController extends BaseController {


    @Autowired
    GardenService gardenService;

    @PostMapping(value = "/addGarden")
    @ApiOperation(value = "/addGarden", notes = "添加年级")
    public Map<String, Object> addGarden(HttpSession session, Garden garden) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        try {
            gardenService.insertSelective(garden);
            return successResult("添加年级成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @DeleteMapping(value = "/deleteGarden")
    @ApiOperation(value = "/deleteGarden", notes = "删除年级")
    public Map<String, Object> deleteGarden(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null == id) {
            return failedResult(200, "id不能为空", null);
        }
        try {
            gardenService.deleteGardenById(id);
            return successResult("删除年级成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }

    }

    @PutMapping(value = "/updateGarden")
    @ApiOperation(value = "/updateGarden", notes = "修改年级")
    public Map<String, Object> updateGarden(HttpSession session, Garden garden) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            gardenService.updateByPrimaryKeySelective(garden);
            return successResult("修改班级成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult("系统错误", null);
        }
    }

    @PostMapping(value = "/selectGarden")
    @ApiOperation(value = "/selectGarden", notes = "查询年级")
    public Map<String, Object> selectGarden(Integer pageSize, Integer pageNum) {
        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<Garden> gardenList = null;
        try {
            gardenList = gardenService.selectGarden();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return successResult("查询成功", new PageInfo<>(gardenList));
    }
}
