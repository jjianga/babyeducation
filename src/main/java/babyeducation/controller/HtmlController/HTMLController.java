package babyeducation.controller.HtmlController;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * describe:
 *
 * @author xieweiyi
 * @date 2019/05/28
 */
@Controller
@RequestMapping("/babyeducation")
@Api(value = "/babyeducation", description = "页面跳转控制")
public class HTMLController {


    @RequestMapping("/login")
    public String login(Model model) {
        model.addAttribute("model", "springboot");
        return "login";
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("left_menu")
    public String leftMenu() {
        return "left_menu";
    }

    @RequestMapping("top_menu")
    public String topMenu() {
        return "top_menu";
    }

    @RequestMapping("/user")
    public String user() {
        return "user";
    }



    @RequestMapping("/user_records")
    public String userRecords() {
        return "user_records";
    }


}


