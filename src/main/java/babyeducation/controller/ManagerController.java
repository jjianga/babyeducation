package babyeducation.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.JedisPool;
import babyeducation.model.Manager;
import babyeducation.selfModel.CurrentUser;
import babyeducation.service.ManagerService;
import babyeducation.service.RSAService;
import babyeducation.utils.MD5Util;
import babyeducation.utils.Utils;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.session.FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME;


@RestController
@RequestMapping(value = "/api/manager")
@Api(value = "/api/manager", description = "管理员相关接口")
public class ManagerController extends BaseController {


    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ManagerService managerService;

    @Autowired
    RedisOperationsSessionRepository sessionRepository;
    @Autowired
    RSAService rsaService;
    @Autowired
    JedisPool jedisPool;

    @PostMapping("/login")
    @ApiOperation(value = "管理员登录", notes = "")
    public Map<String, Object> login(HttpSession session,
                                     @RequestParam String mob,
                                     @RequestParam String pwd,
                                     @RequestParam(required = false) String code) {
        if (Utils.checkNull(mob) || Utils.checkNull(pwd)) {
            return failedResult(200,"缺少入参", null);
        }
//        //验证 验证码
//        Jedis jedis = jedisPool.getResource();
//        String code1 = jedis.get("code");
//        if (!StringUtils.equalsIgnoreCase(code, code1)) {  //忽略验证码大小写
////    	    throw new RuntimeException("验证码对应不上code=" + code + "  sessionCode=" + sessionCode);
//            jedis.del("code");
//            jedis.close();
//            return failedResult("验证码不匹配", null);
//        }
//
//        // 模拟前端进行RSA加密 加密好后丢出password 仅做测试用 如果有前端了就注释掉
//        String publickKey = null;
//        try {
//            publickKey = rsaService.publicKey(mob);
//            PublicKey publicKey1 = RSAUtil.getPublicKey(publickKey);
//            byte[] bytes = RSAUtil.encrypt(pwd.getBytes(), publicKey1);
//            pwd = Base64.encodeBase64String(bytes);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        String finalPwd = null;
        //接到前端传入的RSA加密后的password解密 解密完丢出finalPwd
        try {
            finalPwd = rsaService.getPwd(mob, pwd);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn(RESFailed, session.getId());
            return failedResult(200,"解密失败", null);
        }

        String userInfo = "Manager_" + mob;
        System.out.println(MD5Util.MD5(finalPwd));
        try {
            //拿取用户身份令牌,再将解密后的密码 进行MD5加密 和数据库的MD5密码进行对比
            Authentication authentication = new UsernamePasswordAuthenticationToken(userInfo, MD5Util.MD5(finalPwd));
            //设置用户身份令牌 将令牌添加至 authenticationManager管理
            SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(authentication));
            //清除同名用户信息
            sessionRepository.findByIndexNameAndIndexValue(PRINCIPAL_NAME_INDEX_NAME, userInfo).values()
                    .forEach((Session redisSession) -> {
                        sessionRepository.deleteById(redisSession.getId());
                    });
            //获取用户信息
            CurrentUser currentUser = (CurrentUser) SecurityContextHolder
                    .getContext()
                    .getAuthentication().getPrincipal();
            session.setAttribute("manager", currentUser.getManager());
            Map<String, Object> map = new HashMap<>();
            map.put("manager", currentUser.getManager());
            map.put("token", session.getId());

            //登陆成功后 删除RSA
            rsaService.removeRSA(mob);
            logger.info(loginSuccess, currentUser.getManager().getName(), session.getId());
            Manager managers = (Manager) session.getAttribute("manager");
            return successResult("登录成功", map);
        } catch (BadCredentialsException e) {
            logger.warn("{} ,密码错误", pwd);
            return failedResult("密码错误", null);
        } catch (Exception e) {
            if ("login.error.noIdCard".equals(e.getMessage())) {
                logger.warn("用户不存在异常信息：{}", e.getMessage());
                return failedResult(200,"用户不存在", null);
            }
            if ("login.error.noState".equals(e.getMessage())) {
                logger.warn("用户状态异常信息：{}", e.getMessage());
                return failedResult(200,"用户状态异常", null);
            }
            logger.warn("系统错误异常信息：{}", e.getMessage());
            e.printStackTrace();
            return failedResult(200,"系统错误", null);
        }
    }
    @PostMapping("/loginOut")
    @ApiOperation(value = "管理员退出登录", notes = "")
    public Map<String, Object> loginOut(HttpSession session) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return successResult("退出登录成功", null);
        }
        String userInfo = "Manager_" + manager.getMob();
        //清除同名用户信息
        sessionRepository.findByIndexNameAndIndexValue(PRINCIPAL_NAME_INDEX_NAME, userInfo).values()
                .forEach((Session redisSession) -> {
                    sessionRepository.deleteById(redisSession.getId());
                });
        logger.info(loginOut, manager.getName());
        return successResult("退出登录成功", null);
    }

    @PostMapping("/checkToken")
    @ApiOperation(value = "校验token是否有效", notes = "")
    public Map<String, Object> checkToken(HttpSession session) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200,"身份信息失效", null);
        }
        return successResult("身份信息有效", null);
    }

    @PostMapping("/searchManagerInfo")
    @ApiOperation(value = "获取管理员身份信息", notes = "")
    public Map<String, Object> searchManagerInfo(HttpSession session) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200,"身份信息失效", null);
        }
        try {
            Manager manager1 = managerService.selectByPrimaryKey(manager.getId());
            return successResult("身份信息有效", manager1);

        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200,"系统异常", null);
        }

    }

    @PostMapping("/addManager")
    @ApiOperation(value = "添加管理员身份信息", notes = "姓名，手机号，密码为必填项，权限默认为普通")
    public Map<String, Object> addManager(HttpSession session, Manager manager) {
        Map<String, Object> Map = searchManagerInfo(session);
        if ("failed".equals(Map.get("flag"))) {
            logger.warn("tokenFailed");
            return failedResult(200,"身份信息失效", null);
        }
        //获取用户信息从redis里获得Session信息 如果get到了manager了就说明redis里有token信息
        Manager managers = (Manager) session.getAttribute("manager");
        //通过managers对象get到issupper的数值，为1是超级用户什么都可以做
        if (1 != managers.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200,"权限不足", null);
        }
//        // 模拟前端进行RSA加密 加密好后丢出password 仅做测试用 如果有前端了就注释掉
//        String publickKey = null;
//        try {
//            publickKey = rsaService.publicKey(manager.getMob());
//            PublicKey publicKey1 = RSAUtil.getPublicKey(publickKey);
//            //获取传过来的明文密码 进行RSA加密
//            byte[] bytes = RSAUtil.encrypt(manager.getPwd().getBytes(), publicKey1);
//            //加密完 再塞回model里
//            manager.setPwd(Base64.encodeBase64String(bytes));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        // 接到前端传入的RSA加密后的password解密 解密完丢出finalPwd
        String finalPwd = null;
        try {
            //从modle里获取Mob 电话 和 加密后的密码 ,再进行解密 用finalPwd接住
            String mode = manager.getMob();
            finalPwd = rsaService.getPwd(manager.getMob(), manager.getPwd());
            //解密完后 已经是明文密码 。再塞回model里
            manager.setPwd(finalPwd);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn(RESFailed, e.getMessage());
            return failedResult(200,"解密失败", null);
        }
        //判断入参条件名字 电话号码 和手机号
        if (null == manager.getName() || null == manager.getPwd() || null == manager.getMob()) {
            return failedResult(200,"缺少入参", null);
        }
        //判断电话号码是否正确
        if (!Utils.isMobileNO(manager.getMob())) {
            return failedResult(200,"请输入正确的手机号", manager.getMob());
        }

        int res = managerService.addManger(manager);
        if (-1 == res) {
            logger.warn(addFailed, manager.getName());
            return failedResult(200,"电话已经存在", null);
        }
        if (res > 0) {
            logger.info(addSuccess, manager.getName());
            return successResult("添加成功", null);
        }
        logger.warn(addFailed, manager.getName());
        return failedResult(200,"添加失败", null);

    }

    @PutMapping("/updateManager")
    @ApiOperation(value = "修改管理员身份信息", notes = "")
    public Map<String, Object> updateManager(HttpSession session, Manager manager) {
        Map<String, Object> Map = searchManagerInfo(session);
        if ("failed".equals(Map.get("flag"))) {
            logger.warn("tokenFailed");
            return failedResult(200,"身份信息失效", null);
        }
        Manager managers = (Manager) session.getAttribute("manager");
        int res = 0;
        try {
            Manager a = managerService.selectByPrimaryKey(manager.getId());
            if (a.getMob().equals(manager.getMob())) {
                manager.setMob(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //判断是否为修改个人信息，通过手机号判断如果是则不需要验证权限
        if (manager.getId() == managers.getId()) {
            res = managerService.updateMangerInfo(manager);
            if (-1 == res) {
                logger.warn(updateFailed, manager.getName());
                return failedResult(200,"电话已经存在", null);
            } else if (-2 == res) {
                return failedResult(200,"请输入正确手机号", null);
            } else if (res > 0) {
                logger.info(updateSuccess, manager.getName());
                return successResult("修改成功", null);
            }
            logger.warn(updateFailed, manager.getName());
            return failedResult(200,"修改失败", null);
        }
        //如果不是修改同一个人则判断权权限，权限通过才能修改信息
        if (1 != managers.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200,"权限不足", null);
        }
        Example example = new Example(Manager.class);
        example.or().andEqualTo("id", manager.getId()).andGreaterThanOrEqualTo("state", 0);
        try {
            List<Manager> list = managerService.selectByExample(example);
            if (list.isEmpty()) {
                return failedResult(200,"id不存在", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200,"系统错误", e.getLocalizedMessage());
        }
        res = managerService.updateMangerInfo(manager);
        if (-1 == res) {
            logger.warn(updateFailed, manager.getName());
            return failedResult(200,"电话已经存在", null);
        } else if (-2 == res) {
            return failedResult(200,"请输入正确手机号", null);
        } else if (res > 0) {
            logger.info(updateSuccess, manager.getName());
            return successResult(200,"修改成功", null);
        }
        logger.warn(updateFailed, manager.getName());
        return failedResult(200,"修改失败", null);
    }

    @GetMapping("/selectManager")
    @ApiOperation(value = "查询管理员身份信息", notes = "")
    public Map<String, Object> selectManager(HttpSession session, Manager manager, Integer pageNum,
                                             Integer pageSize) {

        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<Manager> list = managerService.searchManager(manager.getId(), manager.getName(), manager.getMob(), manager.getIsSupper());
        if (null == list) {
            return failedResult(200,"查询失败", null);
        }
        return successResult("查询成功", new PageInfo<>(list));
    }

    @DeleteMapping("/deleteManger")
    @ApiOperation(value = "删除管理员信息", notes = "根据传入数组删除")
    public Map<String, Object> deleteManager(HttpSession session, Integer[] id) {
        Map<String, Object> Map = searchManagerInfo(session);
        if ("failed".equals(Map.get("flag"))) {
            logger.warn("tokenFailed");
            return failedResult(200,"身份信息失效", null);
        }
        Manager managers = (Manager) session.getAttribute("manager");
        if (1 != managers.getIsSupper()) {
            logger.warn(tokenFailed, managers.getName());
            return failedResult(200,"权限不足", null);
        }
        int res = managerService.deleteManger(id);
        if (res > 0) {
            logger.info(deleteSuccess, managers.getName());
            return successResult("删除成功", null);
        }
        logger.info(deleteFailed, managers.getName());
        return failedResult(200,"删除失败", null);
    }

    @PutMapping("/changerPwd")
    @ApiOperation(value = "修改管理员密码", notes = "根据原始密码修改")
    public Map<String, Object> changerPwd(HttpSession session, String oldPwd, String confirmPwd) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200,"身份信息失效", null);
        }
        int id = manager.getId();
        int res = managerService.updatePwd(id, oldPwd, confirmPwd);
        if (res == -1) {
            return failedResult(200,"该用户不存在", null);
        } else if (res == -2) {
            return failedResult(200,"旧密码输入错误", null);
        } else if (res > 0) {
            return successResult("修改密码成功", null);
        }
        return failedResult(200,"修改密码失败", null);
    }

}
