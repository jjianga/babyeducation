package babyeducation.controller;


import babyeducation.model.Manager;
import babyeducation.model.Press;
import babyeducation.service.PressService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/press")
@Api(value = "/api/press", description = "出版社相关接口")
public class PressController extends BaseController {

    @Autowired
    PressService pressService;
    @PostMapping(value = "/addPress")
    @ApiOperation(value = "/addPress", notes = "添加出版社")
    public Map<String, Object> addPress(HttpSession session, Press press) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        try {
            pressService.insertSelective(press);
            return successResult("添加年级成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @DeleteMapping(value = "/deletePress")
    @ApiOperation(value = "/deletePress", notes = "删除出版社")
    public Map<String, Object> deletePress(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null == id) {
            return failedResult(200, "id不能为空", null);
        }
        try {
            pressService.deletePressById(id);
            return successResult("删除出版社成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PutMapping(value = "/updatePress")
    @ApiOperation(value = "/updatePress", notes = "修改出版社")
    public Map<String, Object> updatePress(HttpSession session, Press press) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            pressService.updateByPrimaryKeySelective(press);
            return successResult("修改出版社成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PostMapping(value = "/selectPress")
    @ApiOperation(value = "/selectPress", notes = "查询出版社")
    public Map<String, Object> selectPress(Integer pageNum, Integer pageSize) {
        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        try {
            List<Press> pressList = pressService.selectPress();
            return successResult("查询成功", new PageInfo<>(pressList));
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }

    }
}
