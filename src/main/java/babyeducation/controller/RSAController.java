package babyeducation.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import babyeducation.model.Manager;
import babyeducation.model.RsaInfo;
import babyeducation.service.RSAService;
import babyeducation.utils.ConstantUtil;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api")
@Api(value = "/api", description = "RSA加密相关接口")

public class RSAController extends BaseController {
    @Autowired
    RSAService rsaService;

    @PostMapping("/getPublicKey")
    @ApiOperation("获取公钥，用于登陆/注册/修改密码等")

    public Map<String, Object> getPublicKey(@RequestParam String userInfo) {

        String rsa = rsaService.publicKey(userInfo);
        if (null == rsa) {
            logger.info("用户:{}获取公钥失败", userInfo);
            return failedResult(ConstantUtil.GET_PUBLIC_FLIED, null);

        } else {
            logger.info("用户:{}获取公钥成功", userInfo);
            return successResult(ConstantUtil.GET_PUBLIC_SUCCEED, rsa);
        }
    }


    @GetMapping(value = "/selectPublicKey")
    @ApiOperation("查看公钥，用于超级管理员查看公钥")
    public Map<String, Object> selectPublicKey(String userInfo, String start_time, String end_time, HttpSession session) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult("身份信息失效", null);
        }
        if (3 != manager.getIsSupper()) {
            return failedResult("权限不足", null);
        }
        List<RsaInfo> list = rsaService.selectPublicKey(userInfo, start_time, end_time);

        if (list.isEmpty()) {
            return failedResult("此时间段无记录哦", null);
        } else {
            return successResult("查询成功", list);
        }
    }

}
