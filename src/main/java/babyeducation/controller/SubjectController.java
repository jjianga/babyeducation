package babyeducation.controller;


import babyeducation.model.Manager;
import babyeducation.model.Subject;
import babyeducation.service.SubjectService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/subject")
@Api(value = "/api/subject", description = "科目相关接口")
public class SubjectController extends BaseController {

    @Autowired
    SubjectService subjectService;

    @PostMapping(value = "/addSubject")
    @ApiOperation(value = "/addSubject", notes = "添加科目")
    public Map<String, Object> addSubject(HttpSession session, Subject subject) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            subjectService.insertSelective(subject);
            return successResult("添加科目成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult("系统错误", e.getMessage());
        }
    }

    @DeleteMapping(value = "/deleteSubject")
    @ApiOperation(value = "/deleteSubject", notes = "删除科目")
    public Map<String, Object> deleteSubject(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null == id) {
            return failedResult(200, "id不能为空", null);
        }
        try {
            subjectService.deleteSubjectById(id);
            return successResult("删除科目成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PutMapping(value = "/updateSubject")
    @ApiOperation(value = "/updateSubject", notes = "修改科目")
    public Map<String, Object> updateSubject(HttpSession session, Subject subject) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            subjectService.updateByPrimaryKeySelective(subject);
            return successResult("修改科目成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PostMapping(value = "/selectSubject")
    @ApiOperation(value = "/selectSubject", notes = "查询科目")
    public Map<String, Object> selectSubject(Integer pageNum, Integer pageSize) {

        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        try {
            List<Subject> subjectList = subjectService.selectSubject();
            return successResult("科目查询成功", new PageInfo<>(subjectList));
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

}
