package babyeducation.controller;

import babyeducation.model.UserBookLog;
import babyeducation.service.BookService;
import babyeducation.service.UserBookLogService;
import babyeducation.utils.Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/userBookLog")
@Api(value = "/api/userBookLog", description = "点读课本记录相关接口")
public class UserBookLogController extends BaseController {
    @Autowired
    UserBookLogService userBookLogService;
    @Autowired
    BookService bookService;

//    @PostMapping(value = "/addUserBookLog")
//    @ApiOperation(value = "/addUserBookLog")
//    public Map<String, Object> addUserBookLog(HttpServletRequest req, String mob, UserBookLog userBookLog) {
//        if (Utils.checkNull(mob)) {
//            return failedResult(200, "手机号不能为空", null);
//        }
//        int hasToken = textToken(req, mob);
//        if (1 == hasToken) {
//
//            try {
//                bookService.addReadNumByBookId(userBookLog.getBookId());
//                userBookLogService.updateCreateTimeById(userBookLog.getUserId(), userBookLog.getBookId());
//              return successResult("添加记录成功",null);
//            } catch (Exception e) {
//                e.printStackTrace();
//                return failedResult(200, "系统错误", null);
//            }
//        } else if (0 == hasToken) {
//            return failedResult(10000, "校验码过期请从新登陆", null);
//        } else {
//            return failedResult(10000, "校验码过期请从新登陆", null);
//        }
//    }

    @PostMapping(value = "/selectBookLog")
    @ApiOperation(value = "/selectBookLog",notes = "查询最近阅读")
    public Map<String, Object> selectBookLog(HttpServletRequest req, String mob, Integer userId) {
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        if (null == userId) {
            return failedResult(200, "userId不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            PageHelper.startPage(0, 5);
            try {
                List<UserBookLog> userBookLogList = userBookLogService.selectUserBookLogByUserId(userId);
                new PageInfo<>(userBookLogList);
                return successResult("查询成功", userBookLogList);

            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }

        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }

    }
}
