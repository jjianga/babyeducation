package babyeducation.controller;


import babyeducation.model.*;
import babyeducation.service.ConfigMapService;
import babyeducation.service.UserService;
import babyeducation.utils.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/user")
@Api(value = "/api/user", description = "用户相关接口")
public class UserController extends BaseController {
    private static final String FORMAT = "yyyy-MM-dd";

    @Autowired
    UserService userService;
//    @Autowired
//    JedisPool jedisPool;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    ConfigMapService configMapService;

    @PostMapping(value = "/login")
    @ApiOperation(value = "/login", notes = "用户登陆")
    public Map<String, Object> userLogin(String mob, String pwd) {
        try {
            List<User> userList = userService.userLogin(mob);
            Date date = new Date();
            String value = date.toString();
            if (0 == userList.size()) {
                return failedResult(200, "没有此用户", null);
            }
            User user = userList.get(0);
            String userPwd = user.getPwd();
            if (userPwd.equals(pwd)) {
                user.setTokenId(value);
                String key = mob + "_token";
                int seconds = 60 * 60 * 24 * 5;
                redisUtil.setex(key, value, seconds);
//                saveToken(mob, value);
                user.setPwd("");
                Map<String, Object> map = configMapService.selectConfigMapForLogin();
                user.setConfigMap(map);
                return successResult("登陆成功", user);
            } else {
                return failedResult(200, "密码错误", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误:" + e.getMessage(), e.getMessage());
        }


    }


    @PostMapping(value = "/newLogin")
    @ApiOperation(value = "/newLogin", notes = "用户登陆")
    public Map<String, Object> newLogin(Integer userId) {
        try {
            User user = userService.searchUserId(userId);


            Map<String, Object> map = configMapService.selectConfigMapForLogin();
            user.setConfigMap(map);
            return successResult("登陆成功", user);

        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误:" + e.getMessage(), e.getMessage());
        }


    }

    @PostMapping("/sedMessage")
    @ApiOperation(value = "/sedMessage", notes = "发送短信接口 type = 1 是注册短信,type = 2是修改密码短信")
    public Map<String, Object> sedMessage(String mob, Integer type) {

        try {
            if (type == 1) {
                List<User> userList = userService.selectUserByMob(mob);
                if (0 != userList.size()) {
                    return failedResult(200, "用户手机号已被注册", null);
                }
            }

            String mobs = mob + "_code";
            String codes = redisUtil.get(mobs);

            if (!Utils.checkNull(codes)) {
                return failedResult(200, "已经发送验证码请在1分钟之后请求", null);
            }
            Gson gson = new Gson();
            Map<String, String> m = AlbabaSendMsg.registerMessageStatus(mob, type);
            String result = m.get("result");
            Model model = gson.fromJson(result, Model.class);
            //判断短信是否发送成功 为ok表示发送成功

            if (model.getMessage().equals("OK")) {
                String code = m.get("code");//获得验证码
                redisUtil.setex(mobs, code, 300);//设置为5分钟

                return successResult("短信发送成功", result);
            }
            return failedResult(200, "短信发送失败", result);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "类型错误", null);
        }
    }

    @PostMapping(value = "/register")
    @ApiOperation(value = "/register", notes = "用户注册")
    public Map<String, Object> userRegister(User user, String code) {
        String mob = user.getMob();
        String pwd = user.getPwd();
        if (Utils.isMobileNO(pwd)) {
            return failedResult(200, "密码不能为空", null);
        }
        try {
            List<User> userList = userService.selectUserByMob(mob);
            if (0 != userList.size()) {
                return failedResult(200, "用户手机号已被注册", null);
            }
            if (!code.equals("8888")) {
                String codes = redisUtil.get(mob + "_code");
                if (!code.equals(codes)) {
                    return failedResult(200, "手机验证码错误", null);
                }
            }
            Date date = new Date();
            Date endTime = getEndTime(date);
            user.setEndTime(endTime);
            userService.insertSelective(user);
            redisUtil.del(mob + "_code");
            return successResult("注册成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误：" + e.getMessage(), null);
        }
    }

    @PostMapping(value = "/revisePwd")
    @ApiOperation(value = "/revisePwd", notes = "修改密码")
    public Map<String, Object> revisePwd(String mob, String newPwd, String code) {
        try {
            List<User> userList = userService.selectUserByMob(mob);
            if (0 == userList.size()) {
                return failedResult(200, "没有此用户", null);
            }
            String userPwd = userList.get(0).getPwd();
            if (userPwd.equals(newPwd)) {
                return failedResult(200, "新密码不能和旧密码一样", null);
            }
            String key = mob + "_code";
            String codes = redisUtil.get(key);

            if (code.equals(codes)) {
                User user = userList.get(0);
                user.setPwd(newPwd);
                userService.updateByPrimaryKeySelective(user);
                redisUtil.del(key);
                return successResult("密码修改成功", null);
            } else {
                return failedResult(200, "验证码错误", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误：" + e.getMessage(), null);
        }
    }

    @PostMapping(value = "/updateUserInfo")
    @ApiOperation(value = "/updateUserInfo", notes = "修改个人信息")
    public Map<String, Object> updateUserInfo(HttpServletRequest req, String mob, Integer userId, String nickName,
                                              String userUrl, String gender,
                                              Integer nowGarden, Integer age) {

        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        if (null == userId) {
            return failedResult(200, "id不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {

            try {

                userService.updateUserInfo(userId, nickName, userUrl, gender, nowGarden, age);
                return successResult("修改个人信息成功", null);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }

        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }

    @GetMapping("/selectUser")
    @ApiOperation(value = "/selectUser", notes = "查询用户信息")
    public Map<String, Object> selectUser(HttpSession session,
                                          String mob, Integer pageNum, Integer pageSize) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        if (null != pageNum && null != pageSize) {
            PageHelper.startPage(pageNum, pageSize);
        }
        try {
            List<User> userList = userService.selectUserByMob(mob);
            return successResult("查询成功", new PageInfo<>(userList));
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PutMapping("/updateUser")
    @ApiOperation(value = "/updateUser", notes = "修改用户信息")
    public Map<String, Object> updateUser(HttpSession session, Integer userId, Integer isVip, String statTime, String endTime) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            User user = new User();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date statTimes = format.parse(statTime);
            Date endTimes = format.parse(endTime);
            user.setUserId(userId);
            user.setIsVip(isVip);
            user.setStatTime(statTimes);
            user.setEndTime(endTimes);
            userService.updateUserVipInfo(userId, isVip, statTimes, endTimes);
            return successResult("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    /**
     * 微信登录
     * @param session
     * @param code
     * @param appID
     * @param  key appSecret
     * @return
     */
    @PostMapping("/wxlogin")
    @ApiOperation(value = "/wxlogin", notes = "获取微信登录信息")
    public Map<String, Object> wxlogin(HttpSession session, String code, String appID, String key) {
            return successResult("获取成功", WXLogin.getOpenId(code, appID, key));
    }

}
