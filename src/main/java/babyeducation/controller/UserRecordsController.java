package babyeducation.controller;


import babyeducation.model.Book;
import babyeducation.model.Manager;
import babyeducation.model.UserRecords;
import babyeducation.service.UserRecordsService;
import babyeducation.utils.Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/userRecords")
@Api(value = "/api/userRecords", description = "用户购买记录相关接口")
public class UserRecordsController extends BaseController {

    @Autowired
    UserRecordsService userRecordsService;


    @PostMapping(value = "/addUserRecords")
    @ApiOperation(value = "/addUserRecords", notes = "添加购买记录")
    public Map<String, Object> addUserRecord(HttpSession session, UserRecords userRecords) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            userRecordsService.insetRecord(userRecords);
            return successResult("添加成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PostMapping(value = "/confirm")
    @ApiOperation(value = "/confirm", notes = "修改订单状态")
    public Map<String, Object> confirm(Integer id) {

        try {
            userRecordsService.updateOrderType(id);
            return successResult("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }


    }

    @PostMapping(value = "/selectUserRecords")
    @ApiOperation(value = "/selectUserRecords", notes = "查询个人购买记录")
    public Map<String, Object> selectUserRecords(HttpServletRequest req, String mob, Integer userId, Integer pageNum, Integer pageSize) {

        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            try {
                if (null != pageNum && null != pageSize) {
                    PageHelper.startPage(pageNum, pageSize);
                }
                List<Book> bookList = userRecordsService.selectRecordsByUserId(userId);
                return successResult("查询成功", new PageInfo<>(bookList));
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "查询失败", e.getMessage());
            }
        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }

    @GetMapping("/searchUserRecords")
    @ApiOperation(value = "/searchUserRecords", notes = "查询用户购买书本记录")
    public Map<String, Object> searchUserRecords(HttpSession session, String mob, Integer pageNum, Integer pageSize) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            if (null != pageNum && null != pageSize) {
                PageHelper.startPage(pageNum, pageSize);
            }

            List<UserRecords> userRecordsList = userRecordsService.searchUserRecords(mob);
            return successResult("查询成功", new PageInfo<>(userRecordsList));
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }

    }

    @DeleteMapping(value = "/deleteUserRecords")
    @ApiOperation(value = "/deleteUserRecords", notes = "删除用户记录")
    public Map<String, Object> deleteUserRecords(HttpSession session, Integer id) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            userRecordsService.deleteByPrimaryKey(id);
            return successResult("删除成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }


    }

}
