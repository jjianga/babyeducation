package babyeducation.controller;


import babyeducation.model.UserBookLog;
import babyeducation.model.UserVideoBookLog;
import babyeducation.service.UserVideoBookLogService;
import babyeducation.utils.Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/userVideoBookLog")
@Api(value = "/api/userVideoBookLog", description = "用户同步课程相关接口")
public class UserVideoBookLogController extends BaseController {

    @Autowired
    UserVideoBookLogService userVideoBookLogService;


    @PostMapping(value = "/selectUserBookLog")
    @ApiOperation(value = "/selectUserBookLog", notes = "查询个人同步课程记录")
    public Map<String, Object> selectUserBookLog(HttpServletRequest req, String mob, Integer userId) {

        if (null == userId) {
            return failedResult(200, "userId不能为空", null);
        }
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }

        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            try {
                PageHelper.startPage(1, 5);
                List<UserVideoBookLog> userVideoBookLogList = userVideoBookLogService.selectUserVideoBookLogByUserId(userId);
                new PageInfo<>();
                return successResult("查询成功", userVideoBookLogList);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "查询失败", null);
            }
        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }

    }
}
