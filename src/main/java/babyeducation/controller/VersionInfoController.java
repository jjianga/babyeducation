package babyeducation.controller;

import babyeducation.model.Manager;
import babyeducation.model.VersionInfo;
import babyeducation.service.VersionInfoService;
import babyeducation.utils.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.type.JdbcType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/versionInfo")
@Api(value = "/api/versionInfo", description = "版本信息相关接口")
public class VersionInfoController extends BaseController {


    @Autowired
    VersionInfoService versionInfoService;

    @PostMapping(value = "/addVersionInfo")
    @ApiOperation(value = "/addVersionInfo")
    public Map<String, Object> addVersionInfo(HttpSession session, VersionInfo versioninfo) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        try {
            versionInfoService.insertSelective(versioninfo);
            return successResult("添加成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @DeleteMapping(value = "/deleteVersionInfo")
    @ApiOperation(value = "/deleteVersionInfo")
    public Map<String, Object> deleteVersionInfo(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            versionInfoService.deleteByPrimaryKey(id);
            return successResult("删除成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @PutMapping(value = "/updateVersionInfo")
    @ApiOperation(value = "/updateVersionInfo")
    public Map<String, Object> updateVersionInfo(HttpSession session, VersionInfo versionInfo) {

        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            versionInfoService.updateByPrimaryKeySelective(versionInfo);
            return successResult("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PostMapping(value = "/selectVersionInfo")
    @ApiOperation(value = "/selectVersionInfo")
    public Map<String, Object> selectVersionInfo() {
            try {
                List<VersionInfo> versionInfoList = versionInfoService.selectAll();
                return successResult("查询成功", versionInfoList.get(0));
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }
    }
}
