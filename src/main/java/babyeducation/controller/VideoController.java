package babyeducation.controller;


import babyeducation.model.*;
import babyeducation.service.*;
import babyeducation.utils.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/video")
@Api(value = "/api/video", description = "视频相关接口")
public class VideoController extends BaseController {

    @Autowired
    VideoService videoService;
    @Autowired
    UserService userService;
    @Autowired
    UserRecordsService userRecordsService;
    @Autowired
    BookService bookService;
    @Autowired
    UserVideoBookLogService userVideoBookLogService;


    @PostMapping(value = "/addVideo")
    @ApiOperation(value = "/addVideo", notes = "添加视频接口")
    public Map<String, Object> addVideo(HttpSession session, Video video) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        try {
            videoService.insertSelective(video);
            return successResult("添加成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }

    @DeleteMapping(value = "/deleteVideo")
    @ApiOperation(value = "/deleteVideo", notes = "删除视频接口")
    public Map<String, Object> deleteVideo(HttpSession session, Integer id) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }

        if (null == id) {
            return failedResult(200, "id不能为空", null);

        }
        try {
            videoService.deleteVideoById(id);
            return successResult("删除成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", null);
        }
    }

    @PutMapping(value = "/updateVideo")
    @ApiOperation(value = "/updateVideo", notes = "修改视频")
    public Map<String, Object> updateVideo(HttpSession session, Video video) {
        Manager manager = (Manager) session.getAttribute("manager");
        if (null == manager) {
            return failedResult(200, "身份失效", null);
        }
        if (1 != manager.getIsSupper()) {
            logger.warn(tokenFailed, manager.getName());
            return failedResult(200, "权限不足", null);
        }
        try {
            videoService.updateByPrimaryKeySelective(video);
            return successResult("修改成功", null);
        } catch (Exception e) {
            e.printStackTrace();
            return failedResult(200, "系统错误", e.getMessage());
        }
    }


    @PostMapping(value = "/selectVideoById")
    @ApiOperation(value = "/selectVideoById", notes = "根据ID查询视频")
    public Map<String, Object> selectVideoById(HttpServletRequest req, String mob, Integer videoId) {
        if (Utils.checkNull(mob)) {
            return failedResult(200, "手机号不能为空", null);
        }
        int hasToken = textToken(req, mob);
        if (1 == hasToken) {
            try {
                List<User> userList = userService.userLogin(mob);
                User user = userList.get(0);
                int hasVip = user.getIsVip();
                Video video = videoService.selectVideoByVideoId(videoId);
                Integer isFree = video.getIsFree();
                if (0 == hasVip) {
                    if (0 == isFree) {
                        Integer userId = user.getUserId();
                        List<UserRecords> userRecordsList = userRecordsService.hasVideoByUserId(userId, videoId);
                        if (0 == userRecordsList.size()) {
                            video.setVideoUrl("");
                            video.setIsVip(hasVip);
                            return successResult( "请购买或者充值VIP会员", video);
                        }
                    }
                }
                video.setIsVip(hasVip);
                return successResult("查询成功", video);
            } catch (Exception e) {
                e.printStackTrace();
                return failedResult(200, "系统错误", null);
            }

        } else if (0 == hasToken) {
            return failedResult(10000, tokensFailed, null);
        } else {
            return failedResult(10000, tokensFailed, null);
        }
    }
}
