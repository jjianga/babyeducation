package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.BannerConfig;

import java.util.List;

public interface BannerConfigMapper extends BaseMapper<BannerConfig> {

    List<BannerConfig> selectBannerConfig();

}
