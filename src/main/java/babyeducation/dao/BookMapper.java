package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Book;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface BookMapper extends BaseMapper<Book> {

    List<Book> selectBookForHomePage(@Param("gardenId") Integer gardenId,
                                     @Param("pressId") Integer pressId,
                                     @Param("subjectId") Integer subjectId,
                                     @Param("volume") Integer volume,
                                     @Param("bookName") String bookName,
                                     @Param("hasVideo") Integer hasVideo,
                                     @Param("hasBook") Integer hasBook);

    void deleteBookById(@Param("id") Integer id);

    Book selectBookById(@Param("bookId") Integer bookId);

    int addReadNumByBookId(@Param("bookId") Integer bookId);


    Book selectBookInfoByVideoId(@Param("videoId") Integer videoId);

    List<Book> selectUserRecordsById(@Param("userId") Integer userId);

    List<Book> newSelectBookBase(@Param("gardenId") Integer gardenId,
                                 @Param("pressId") Integer pressId,
                                 @Param("subjectId") Integer subjectId,
                                 @Param("volume") Integer volume,
                                 @Param("hasVideo") Integer hasVideo,
                                 @Param("hasBook") Integer hasBook);

}
