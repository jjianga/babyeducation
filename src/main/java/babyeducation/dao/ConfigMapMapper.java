package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.ConfigMap;

import java.util.List;
import java.util.Map;

public  interface ConfigMapMapper extends BaseMapper<ConfigMap> {

    List<ConfigMap> selectConfigMap();

    ConfigMap selectConfigMapByKey(String key);


}
