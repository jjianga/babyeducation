package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.EBook;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EBookMapper extends BaseMapper<EBook> {



    List<EBook> selectEBookByParameter(@Param("gardenId") Integer gardenId,
                                       @Param("subjectId") Integer subjectId,
                                       @Param("EType") Integer eType);

    List<EBook> selectEBookNoEType(@Param("gardenId") Integer gardenId,
                                   @Param("subjectId") Integer subjectId);

}
