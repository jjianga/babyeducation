package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Garden;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GardenMapper extends BaseMapper<Garden> {

    void deleteGardenById(@Param("id") Integer id);

    List<Garden> selectGarden();
}
