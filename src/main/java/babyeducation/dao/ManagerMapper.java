package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Manager;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ManagerMapper extends BaseMapper<Manager> {

    List<Manager> searchManager(@Param("id") Integer id, @Param("name") String name,
                                @Param("mob") String mob,
                                @Param("isSupper") Integer isSupper);

    int updateManager(@Param("id") Integer id);
}
