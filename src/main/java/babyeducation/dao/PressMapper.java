package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Press;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PressMapper extends BaseMapper<Press> {

    void deletePressById(@Param("id") Integer id);

    List<Press> selectPress();

}
