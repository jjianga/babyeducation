package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.RsaInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RsaInfoMapper extends BaseMapper<RsaInfo> {


    List<RsaInfo> selectPublicKey(@Param("userInfo") String userInfo, @Param("startTime") String start_time, @Param("endTime") String end_time);

}
