package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Subject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SubjectMapper extends BaseMapper<Subject> {

    void deleteSubjectById(@Param("id") Integer id);

    List<Subject> selectSubject();

}
