package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.UserBookLog;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

public interface UserBookLogMapper extends BaseMapper<UserBookLog> {

    void updateCreateTimeById(@Param("time") Timestamp time, @Param("id") Integer id);

    List<UserBookLog> selectUserBookLogByUserId(@Param("userId") Integer userId);
}
