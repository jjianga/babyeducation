package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.Date;
import java.util.List;

public interface UserMapper extends BaseMapper<User> {

    List<User> selectUserByMob(@Param("mob") String mob) throws Exception;

    void clearVip(@Param("userId") Integer userId );

    int updateUserInfo(@Param("userId") Integer userId,
                       @Param("nickName") String nickName,
                       @Param("userUrl") String userUrl,
                       @Param("gender")String gender,
                       @Param("nowGarden")Integer nowGarden,
                       @Param("age")Integer age);

    int updateUserVipInfo(@Param("isVip") Integer isVip,
            @Param("userId") Integer userId,
                          @Param("statTime")Date statTime,
                          @Param("endTime") Date endTime);


    User searchUserId(@Param("userId") Integer userId);

    int newUserBy(@Param("user") User user);


}
