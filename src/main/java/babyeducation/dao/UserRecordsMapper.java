package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Book;
import babyeducation.model.UserRecords;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface UserRecordsMapper extends BaseMapper<UserRecords> {




    List<UserRecords> isRecordsByUserId(@Param("userId")Integer userId,
                                        @Param("bookId") Integer BookId);

    List<UserRecords> hasVideoByUserId(@Param("userId") Integer userId,
                                       @Param("videoId") Integer videoId);

    void insetRecords(@Param("userRecords") UserRecords userRecords);


    int updateOrderType(@Param("orderId") Integer orderId) ;

    List<UserRecords> selectUserRecords(@Param("mob") String mob);
}
