package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.UserBookLog;
import babyeducation.model.UserVideoBookLog;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

public interface UserVideoBookLogMapper extends BaseMapper<UserVideoBookLog> {

    void updateCreateTimeById(@Param("time") Timestamp time, @Param("id") Integer id);

    List<UserVideoBookLog> selectUserVideoBookLogByUserId(@Param("userId") Integer userId);
}
