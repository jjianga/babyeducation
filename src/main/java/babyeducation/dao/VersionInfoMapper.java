package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.VersionInfo;

public interface VersionInfoMapper extends BaseMapper<VersionInfo> {
}