package babyeducation.dao;

import babyeducation.db.BaseMapper;
import babyeducation.model.Video;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

public interface VideoMapper extends BaseMapper<Video> {

    List<Video> selectVideoByVbuId(@Param("bookId") Integer bookId);

    void deleteVideoById(@Param("id") Integer id);

    Video selectVideoByVideoId(@Param("videoId") Integer videoId);
}
