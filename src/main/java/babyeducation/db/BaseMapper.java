package babyeducation.db;

import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * BaseMapper
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
@Service
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
