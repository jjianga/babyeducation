package babyeducation.db;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * MybatisConfig
 *
 * @author xieweyi
 * @date 2019/5/28
 */
@Configuration //表示这是一个配置类，项目运行时框架会先扫描配置类
@MapperScan(basePackages = "babyeducation.dao") //指定dao层地址
public class MybatisConfig {

    /**
     * 在Spring中,处理外部值的最简单方式就是声明属性源并通过Spring的Environment来检索属性
     */
    @Autowired
    private Environment environment;

    /**
     * 创建数据源
     *
     * @return
     * @throws Exception
     */
    @Bean //表示这是配置类中的一个配置项
    @Primary //唯一的
    public DataSource createDataSource() throws Exception {
        //配置清单
        Properties properties = new Properties();
        //装载MySql驱动
        properties.put("driverClassName", environment.getProperty("jdbc.driverClassName"));
        properties.put("url", environment.getProperty("jdbc.url"));
        properties.put("username", environment.getProperty("jdbc.username"));
        properties.put("password", environment.getProperty("jdbc.password"));
        //插入表情设置
        properties.put("initConnectionSqls",environment.getProperty("jdbc.connection-init-sql"));
        return DruidDataSourceFactory.createDataSource(properties);
    }

    /**
     * 根据数据源创建SqlSession工厂
     */
    @Bean
    public SqlSessionFactory sessionFactory(DataSource ds) throws Exception {
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        factory.setDataSource(ds);

        //配置分页插件
        PageHelper pageHelper = new PageHelper();
        Properties properties = new Properties();
        //开启分页参数合理化
        //默认是false  如果给的页数超出了范围 给空数组
        //开启之后  如果超出  小于0 给第一页  大于最大值给 最后一页
        properties.setProperty("reasonable", "true");
        //默认false  插件会从方法的参数中自动根据param配置字段取值,自动分页
        properties.setProperty("supportMethodsArguments", "true");
        //开启检查是否返回的对象是PageInfo
        properties.setProperty("returnPageInfo", "check");
        pageHelper.setProperties(properties);
        //添加插件
        factory.setPlugins(new Interceptor[]{pageHelper});

        //指定model层位置
        factory.setTypeAliasesPackage(environment.getProperty("mybatis.type-aliases-package"));
        //指定SqlMapper层位置
        factory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(environment.getProperty("mybatis.mapper-locations")));
        return factory.getObject();
    }

    /**
     * 配置数据事务管理器
     *
     * @param ds
     * @return
     */
    @Bean
    public DataSourceTransactionManager transactionManager(DataSource ds) {
        return new DataSourceTransactionManager(ds);
    }


}
