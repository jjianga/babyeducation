package babyeducation.db;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

import java.util.Properties;

@Configuration //配置类
@AutoConfigureAfter(MybatisConfig.class) //设置该配置类MybatisConfig类后面再扫描
public class MybatisMapperScannerConfig {

    /**
     * tk的mapper扫描配置
     */
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        //设置SqlSession工厂
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        //这是dao层文件位置
        mapperScannerConfigurer.setBasePackage("babyeducation.dao");
        //配置基础信息
        Properties properties = new Properties();
        //设置通用Mapper的位置
        properties.setProperty("mappers", "babyeducation.db.BaseMapper");
        properties.setProperty("notEmpty", "false");
        properties.setProperty("IDENTITY", "MYSQL");
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;

    }

}
