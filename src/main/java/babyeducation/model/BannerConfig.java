package babyeducation.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "banner_config")
public class BannerConfig {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 推荐课本ID
     */
    @Column(name = "book_id")
    private Integer bookId;



    /**
     * 图片地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    @Transient
    @Column(name = "book_name")
    private String bookName;

    @Transient
    @Column(name = "press_id")
    private Integer pressId;

    @Transient
    @Column(name = "press_name")
    private String pressName;

    @Transient
    @Column(name = "garden_id")
    private Integer gardenId;
    /**
     * 年级名称
     */
    @Transient
    @Column(name = "garden_name")
    private String gardenName;

    /**
     * 科目ID
     */
    @Transient
    @Column(name = "subject_id")
    private Integer subjectId;


    @Transient
    @Column(name = "subject_name")
    private String subjectName;

    @Transient
    @Column(name = "cover_image_url")
    private String coverImageUrl;

    @Transient
    @Column(name = "has_video")
    private Integer hasVideo;

    @Transient
    @Column(name = "has_book")
    private Integer hasBook;

    @Transient
    private Integer volume;

    @Transient
    @Column(name = "read_number")
    private Long readNumber;

    public String getBookName() {
        return bookName == null ? "" : bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getPressId() {
        return pressId;
    }

    public void setPressId(Integer pressId) {
        this.pressId = pressId;
    }

    public String getPressName() {
        return pressName == null ? "" : pressName;
    }

    public void setPressName(String pressName) {
        this.pressName = pressName;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public String getGardenName() {
        return gardenName == null ? "" : gardenName;
    }

    public void setGardenName(String gardenName) {
        this.gardenName = gardenName;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName == null ? "" : subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCoverImageUrl() {
        return coverImageUrl == null ? "" : coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public Integer getHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(Integer hasVideo) {
        this.hasVideo = hasVideo;
    }

    public Integer getHasBook() {
        return hasBook;
    }

    public void setHasBook(Integer hasBook) {
        this.hasBook = hasBook;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Long getReadNumber() {
        return readNumber;
    }

    public void setReadNumber(Long readNumber) {
        this.readNumber = readNumber;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getIsFree() {
        return isFree;
    }

    public void setIsFree(Integer isFree) {
        this.isFree = isFree;
    }

    @Transient
    private float price;

    @Transient
    private Integer isFree;

    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 1：正常 -1：删除 0：已读
     */
    private Byte state;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取推荐课本ID
     *
     * @return book_id - 推荐课本ID
     */
    public Integer getBookId() {
        return bookId;
    }

    /**
     * 设置推荐课本ID
     *
     * @param bookId 推荐课本ID
     */
    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }



    /**
     * 获取图片地址
     *
     * @return img_url - 图片地址
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * 设置图片地址
     *
     * @param imgUrl 图片地址
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取1：正常 -1：删除 0：已读
     *
     * @return state - 1：正常 -1：删除 0：已读
     */
    public Byte getState() {
        return state;
    }

    /**
     * 设置1：正常 -1：删除 0：已读
     *
     * @param state 1：正常 -1：删除 0：已读
     */
    public void setState(Byte state) {
        this.state = state;
    }
}
