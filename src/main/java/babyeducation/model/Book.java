package babyeducation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "book")
public class Book implements Serializable {
    /**
     * 主键ID 自增
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    @Column(name = "book_id")
    private Integer bookId;

    /**
     * 课本名称
     */
    @Column(name = "book_name")
    private String bookName;


    /**
     * 出版社ID
     */
    @Column(name = "press_id")
    private Integer pressId;

    /**
     * 出版社名称
     */
    @Transient
    @Column(name = "press_name")
    private String pressName;

    /**
     * 年级ID
     */
    @Column(name = "garden_id")
    private Integer gardenId;
    /**
     * 年级名称
     */
    @Transient
    @Column(name = "garden_name")
    private String gardenName;

    @Transient
    private Integer isVip;

    public Integer getIsVip() {
        return isVip;
    }

    public void setIsVip(Integer isVip) {
        this.isVip = isVip;
    }

    /**
     * 科目ID
     */
    @Column(name = "subject_id")
    private Integer subjectId;


    /**
     * 科目名称
     */
    @Transient
    @Column(name = "subject_name")
    private String subjectName;

    private float price;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Transient
    private boolean canRead;

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    /**
     * 课本封面url
     */
    @Column(name = "cover_image_url")
    private String coverImageUrl;
    private Integer isFree;

    public Integer getIsFree() {
        return isFree;
    }

    public void setIsFree(Integer isFree) {
        this.isFree = isFree;
    }

    /**
     * 视频下载url
     */
    @Column(name = "down_url")
    private String downUrl;

    @Column(name = "course_id")
    private Integer courseId;

    @Column(name = "has_book")
    private Integer hasBook;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getHasBook() {
        return hasBook;
    }

    public void setHasBook(Integer hasBook) {
        this.hasBook = hasBook;
    }

    /**
     * 0是没有 1是有
     */
    @Column(name = "has_video")
    private Integer hasVideo;

    /**
     * 0是上册 1是下册
     */
    private Integer volume;

    /**
     * 阅读量
     */
    @Column(name = "read_number")
    private Long readNumber;



    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    public String getPressName() {
        return pressName == null ? "" : pressName;
    }

    public void setPressName(String pressName) {
        this.pressName = pressName;
    }

    public String getGardenName() {
        return gardenName == null ? "" : gardenName;
    }

    public void setGardenName(String gardenName) {
        this.gardenName = gardenName;
    }

    public String getSubjectName() {
        return subjectName == null ? "" : subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName == null ? "" : bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getPressId() {
        return pressId;
    }

    public void setPressId(Integer pressId) {
        this.pressId = pressId;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getCoverImageUrl() {
        return coverImageUrl == null ? "" : coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getDownUrl() {
        return downUrl == null ? "" : downUrl;
    }

    public void setDownUrl(String downUrl) {
        this.downUrl = downUrl;
    }

    public Integer getHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(Integer hasVideo) {
        this.hasVideo = hasVideo;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Long getReadNumber() {
        return readNumber;
    }

    public void setReadNumber(Long readNumber) {
        this.readNumber = readNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    /**
     * 1：正常 -1：删除 0：已读
     */
    private Byte state;

}
