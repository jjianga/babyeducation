package babyeducation.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "e_book")
public class EBook implements Serializable {
    /**
     * 主键ID
     */
    @Id
    @Column(name = "e_book_id")
    private Integer eBookId;

    /**
     * 推广书本名称
     */
    @Column(name = "e_book_name")
    private String eBookName;

    /**
     * 年级ID
     */
    @Column(name = "garden_id")
    private Integer gardenId;

    /**
     * 科目ID
     */
    @Column(name = "subject_id")
    private Integer subjectId;

    /**
     * 0 是所有 1 是同步课， 2是专题课
     */
    @Column(name = "e_type")
    private Integer eType;

    /**
     * 购买人数
     */
    @Column(name = "buy_num")
    private Long buyNum;

    /**
     * 课本描述
     */
    @Column(name = "e_book_describe")
    private String eBookDescribe;

    /**
     * 微信二维码描述
     */
    @Column(name = "wx_code_describe")
    private String wxCodeDescribe;

    /**
     * 微信公众号名称
     */
    @Column(name = "wx_public_address")
    private String wxPublicAddress;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 1：正常 -1：删除 0：已读
     */
    private Byte state;

    /**
     * 课本封面
     */
    @Column(name = "e_book_cover")
    private String eBookCover;

    /**
     * 横幅图片
     */
    @Column(name = "banner_img")
    private String bannerImg;

    /**
     * 微信二维码图片地址
     */
    @Column(name = "wx_code_img")
    private String wxCodeImg;

    /**
     * 获取主键ID
     *
     * @return e_book_id - 主键ID
     */
    public Integer geteBookId() {
        return eBookId;
    }

    /**
     * 设置主键ID
     *
     * @param eBookId 主键ID
     */
    public void seteBookId(Integer eBookId) {
        this.eBookId = eBookId;
    }

    /**
     * 获取推广书本名称
     *
     * @return e_book_name - 推广书本名称
     */
    public String geteBookName() {
        return eBookName;
    }

    /**
     * 设置推广书本名称
     *
     * @param eBookName 推广书本名称
     */
    public void seteBookName(String eBookName) {
        this.eBookName = eBookName == null ? null : eBookName.trim();
    }

    /**
     * 获取年级ID
     *
     * @return garden_id - 年级ID
     */
    public Integer getGardenId() {
        return gardenId;
    }

    /**
     * 设置年级ID
     *
     * @param gardenId 年级ID
     */
    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    /**
     * 获取科目ID
     *
     * @return subject_id - 科目ID
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * 设置科目ID
     *
     * @param subjectId 科目ID
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * 获取0 是所有 1 是同步课， 2是专题课
     *
     * @return e_type - 0 是所有 1 是同步课， 2是专题课
     */
    public Integer geteType() {
        return eType;
    }

    /**
     * 设置0 是所有 1 是同步课， 2是专题课
     *
     * @param eType 0 是所有 1 是同步课， 2是专题课
     */
    public void seteType(Integer eType) {
        this.eType = eType;
    }

    /**
     * 获取购买人数
     *
     * @return buy_num - 购买人数
     */
    public Long getBuyNum() {
        return buyNum;
    }

    /**
     * 设置购买人数
     *
     * @param buyNum 购买人数
     */
    public void setBuyNum(Long buyNum) {
        this.buyNum = buyNum;
    }

    /**
     * 获取课本描述
     *
     * @return e_book_describe - 课本描述
     */
    public String geteBookDescribe() {
        return eBookDescribe;
    }

    /**
     * 设置课本描述
     *
     * @param eBookDescribe 课本描述
     */
    public void seteBookDescribe(String eBookDescribe) {
        this.eBookDescribe = eBookDescribe == null ? null : eBookDescribe.trim();
    }

    /**
     * 获取微信二维码描述
     *
     * @return wx_code_describe - 微信二维码描述
     */
    public String getWxCodeDescribe() {
        return wxCodeDescribe;
    }

    /**
     * 设置微信二维码描述
     *
     * @param wxCodeDescribe 微信二维码描述
     */
    public void setWxCodeDescribe(String wxCodeDescribe) {
        this.wxCodeDescribe = wxCodeDescribe == null ? null : wxCodeDescribe.trim();
    }

    /**
     * 获取微信公众号名称
     *
     * @return wx_public_address - 微信公众号名称
     */
    public String getWxPublicAddress() {
        return wxPublicAddress;
    }

    /**
     * 设置微信公众号名称
     *
     * @param wxPublicAddress 微信公众号名称
     */
    public void setWxPublicAddress(String wxPublicAddress) {
        this.wxPublicAddress = wxPublicAddress == null ? null : wxPublicAddress.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取1：正常 -1：删除 0：已读
     *
     * @return state - 1：正常 -1：删除 0：已读
     */
    public Byte getState() {
        return state;
    }

    /**
     * 设置1：正常 -1：删除 0：已读
     *
     * @param state 1：正常 -1：删除 0：已读
     */
    public void setState(Byte state) {
        this.state = state;
    }

    /**
     * 获取课本封面
     *
     * @return e_book_cover - 课本封面
     */
    public String geteBookCover() {
        return eBookCover;
    }

    /**
     * 设置课本封面
     *
     * @param eBookCover 课本封面
     */
    public void seteBookCover(String eBookCover) {
        this.eBookCover = eBookCover == null ? null : eBookCover.trim();
    }

    /**
     * 获取横幅图片
     *
     * @return banner_img - 横幅图片
     */
    public String getBannerImg() {
        return bannerImg;
    }

    /**
     * 设置横幅图片
     *
     * @param bannerImg 横幅图片
     */
    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg == null ? null : bannerImg.trim();
    }

    /**
     * 获取微信二维码图片地址
     *
     * @return wx_code_img - 微信二维码图片地址
     */
    public String getWxCodeImg() {
        return wxCodeImg;
    }

    /**
     * 设置微信二维码图片地址
     *
     * @param wxCodeImg 微信二维码图片地址
     */
    public void setWxCodeImg(String wxCodeImg) {
        this.wxCodeImg = wxCodeImg == null ? null : wxCodeImg.trim();
    }
}
