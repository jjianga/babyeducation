package babyeducation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "manager")
public class Manager implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 管理员名称
     */
    private String name;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 手机号
     */
    private String mob;

    /**
     * 1,超级管理，0普通
     */
    @Column(name = "is_supper")
    private Integer isSupper;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 1：正常 -1：删除 0：禁用
     */
    private Integer state;

    /**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取管理员名称
     *
     * @return name - 管理员名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置管理员名称
     *
     * @param name 管理员名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取密码
     *
     * @return pwd - 密码
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * 设置密码
     *
     * @param pwd 密码
     */
    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    /**
     * 获取手机号
     *
     * @return mob - 手机号
     */
    public String getMob() {
        return mob;
    }

    /**
     * 设置手机号
     *
     * @param mob 手机号
     */
    public void setMob(String mob) {
        this.mob = mob == null ? null : mob.trim();
    }

    /**
     * 获取1,超级管理，0普通
     *
     * @return is_supper - 1,超级管理，0普通
     */
    public Integer getIsSupper() {
        return isSupper;
    }

    /**
     * 设置1,超级管理，0普通
     *
     * @param isSupper 1,超级管理，0普通
     */
    public void setIsSupper(Integer isSupper) {
        this.isSupper = isSupper;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取1：正常 -1：删除 0：禁用
     *
     * @return state - 1：正常 -1：删除 0：禁用
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置1：正常 -1：删除 0：禁用
     *
     * @param state 1：正常 -1：删除 0：禁用
     */
    public void setState(Integer state) {
        this.state = state;
    }
}
