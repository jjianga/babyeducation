package babyeducation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.*;

@Table(name = "user")
public class User implements Serializable {
    /**
     * 用户ID
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 用户头像
     */
    @Column(name = "user_url")
    private String userUrl;

    /**
     * 电话号码
     */
    private String mob;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 用户昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    public Map<String, Object> getConfigMap() {
        return configMap;
    }

    public void setConfigMap(Map<String, Object> configMap) {
        this.configMap = configMap;
    }

    @Transient
    private Map<String,Object> configMap;

    /**
     * 性别
     */
    private String gender;

    /**
     * 目前所在年级
     */
    @Column(name = "now_garden")
    private Integer nowGarden;


    @Transient
    @Column(name = "garden_name")
    private String gardenName;

    public String getGardenName() {
        return gardenName == null ? "" : gardenName;
    }

    public void setGardenName(String gardenName) {
        this.gardenName = gardenName;
    }

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 是否为vip (0不是 1是)
     */
    @Column(name = "is_vip")
    private Integer isVip;

    /**
     * vip开始时间
     */
    @Column(name = "stat_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date statTime;

    /**
     * vip结束时间
     */
    @Column(name = "end_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;


    /**
     * 用户token
     */
    @Transient
    private String tokenId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 1：正常 -1：删除 0：已读
     */
    private Byte state;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserUrl() {
        return userUrl == null ? "" : userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public String getMob() {
        return mob == null ? "" : mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getPwd() {
        return pwd == null ? "" : pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getNickName() {
        return nickName == null ? "" : nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGender() {
        return gender == null ? "" : gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public Integer getNowGarden() {
        return nowGarden;
    }

    public void setNowGarden(Integer nowGarden) {
        this.nowGarden = nowGarden;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getIsVip() {
        return isVip;
    }

    public void setIsVip(Integer isVip) {
        this.isVip = isVip;
    }

    public Date getStatTime() {
        return statTime;
    }

    public void setStatTime(Date statTime) {
        this.statTime = statTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getTokenId() {
        return tokenId == null ? "" : tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }
}
