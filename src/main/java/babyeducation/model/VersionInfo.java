package babyeducation.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "version_info")
public class VersionInfo {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 版本编号
     */
    @Column(name = "version_no")
    private String versionNo;

    /**
     * 版本名称
     */
    @Column(name = "version_name")
    private String versionName;

    /**
     * 下载地址
     */
    @Column(name = "down_path")
    private String downPath;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0 是不强制，1是强制
     */
    @Column(name = "forced_type")
    private Integer forcedType;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 1：正常 -1：删除 0：已读
     */
    private Byte state;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取版本编号
     *
     * @return version_no - 版本编号
     */
    public String getVersionNo() {
        return versionNo;
    }

    /**
     * 设置版本编号
     *
     * @param versionNo 版本编号
     */
    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo == null ? null : versionNo.trim();
    }

    /**
     * 获取版本名称
     *
     * @return version_name - 版本名称
     */
    public String getVersionName() {
        return versionName;
    }

    /**
     * 设置版本名称
     *
     * @param versionName 版本名称
     */
    public void setVersionName(String versionName) {
        this.versionName = versionName == null ? null : versionName.trim();
    }

    /**
     * 获取下载地址
     *
     * @return down_path - 下载地址
     */
    public String getDownPath() {
        return downPath;
    }

    /**
     * 设置下载地址
     *
     * @param downPath 下载地址
     */
    public void setDownPath(String downPath) {
        this.downPath = downPath == null ? null : downPath.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取0 是不强制，1是强制
     *
     * @return forced_type - 0 是不强制，1是强制
     */
    public Integer getForcedType() {
        return forcedType;
    }

    /**
     * 设置0 是不强制，1是强制
     *
     * @param forcedType 0 是不强制，1是强制
     */
    public void setForcedType(Integer forcedType) {
        this.forcedType = forcedType;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取1：正常 -1：删除 0：已读
     *
     * @return state - 1：正常 -1：删除 0：已读
     */
    public Byte getState() {
        return state;
    }

    /**
     * 设置1：正常 -1：删除 0：已读
     *
     * @param state 1：正常 -1：删除 0：已读
     */
    public void setState(Byte state) {
        this.state = state;
    }
}