package babyeducation.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionIdResolver;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * RedisSessionConfig
 * redis 托管 http-session
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 2592000) //设置session失效时间
public class RedisSessionConfig {
    final Logger logger = LoggerFactory.getLogger(RedisSessionConfig.class);

    /**
     * 配置一下请求头的session head名称
     *
     * @return
     */
    @Bean
    public HeaderHttpSessionIdResolver httpSessionStrategy() {
        return new HeaderHttpSessionIdResolver("x-auth-token");
    }


    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.timeout}")
    private int timeout;


    @Value("${spring.redis.database}")
    Integer redisDatabase;

    @Value("${spring.redis.jedis.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.jedis.pool.max-wait}")
    private long maxWaitMillis;

    @Value("${spring.redis.password}")
    private String password;

    @Value("${redisCleanSuccess}")
    private String redisCleanSuccess;
    @Value("${redisCleanFailed}")
    private String redisCleanFailed;
    @Value("${redisCleanException}")
    private String redisCleanException;

//    @Value("${spring.redis.block-when-exhausted}")
//    private boolean  blockWhenExhausted;
    /**
     * 注入JedisPool 拿到jedis连接信息
     *
     * @return
     * @throws Exception
     */
    @Bean
    public JedisPool redisPoolFactory() throws Exception {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(-1);
        jedisPoolConfig.setMaxIdle(5);

        jedisPoolConfig.setMaxWaitMillis(1000*100);
        // 连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认true
        //在borrow(引入)一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
        jedisPoolConfig.setTestOnBorrow(true);
        //return 一个jedis实例给pool时，是否检查连接可用性（ping()）
        jedisPoolConfig.setTestOnReturn(true);

        jedisPoolConfig.setBlockWhenExhausted(true);
        // 是否启用pool的jmx管理功能, 默认true
        jedisPoolConfig.setJmxEnabled(true);
        logger.info("jedisPool注入成功");
        // 如果redis 没有密码走 就不要进if 有密码就走下面 区分是不是使用云端redis
        if ("".equals(password) || null == password) {
            return new JedisPool(jedisPoolConfig, host, port, timeout);
        }
        return new JedisPool(jedisPoolConfig, host, port, timeout, password);
    }

    /**
     * 启动项目时清空redis信息
     *
     * @return
     */
    @Bean
    public void cleanRedis() {
        Jedis jedis = null;
        try {

            jedis = redisPoolFactory().getResource();
            jedis.select(redisDatabase);
            logger.info(redisCleanSuccess, redisDatabase);
            jedis.flushDB();
            jedis.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(redisCleanException, redisDatabase, e.getMessage());
        }
        logger.info(redisCleanFailed, redisDatabase);
    }




}
