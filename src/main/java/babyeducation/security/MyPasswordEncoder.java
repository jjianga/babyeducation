package babyeducation.security;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * MyPasswordEncoder
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public class MyPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.toString().equals(encodedPassword);
    }
}
