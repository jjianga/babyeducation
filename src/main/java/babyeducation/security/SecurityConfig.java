package babyeducation.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.savedrequest.NullRequestCache;
import babyeducation.service.MyUserDetailsService;

/**
 * SecurityConfig
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    MyUserDetailsService myUserDetailsService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .authorizeRequests()
                .antMatchers("/api/**")
                .permitAll()
//                .antMatchers("/garden/*")
//                //需要身份认证
//                .authenticated()
                .and()
                .requestCache()
                //初始化一个cache
                .requestCache(new NullRequestCache())
                .and()
                //添加session管理
                //sessionCreationPolicy 配置http-session拦截机制
                //SessionCreationPolicy.NEVER 从不拦截
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                //设置最大session会话数
                .maximumSessions(1)
                //禁止最大会话数登录
                .maxSessionsPreventsLogin(false);
        http.csrf().disable();//禁用csrf保护
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService)
                .passwordEncoder(new MyPasswordEncoder());
    }


}
