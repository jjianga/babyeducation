package babyeducation.selfModel;

import org.springframework.security.core.GrantedAuthority;
import babyeducation.model.Manager;

import java.util.Collection;

/**
 * CurrentUser
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private Manager manager;

    public CurrentUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public void setUsers(Manager manager) {
        this.manager = manager;
    }

    public Manager getManager() {
        return manager;
    }
}
