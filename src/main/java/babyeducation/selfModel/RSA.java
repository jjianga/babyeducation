package babyeducation.selfModel;

/**
 * RSA
 *
 * @author HuYueling
 * @date 2019/3/20
 */
public class RSA {
    /**
     * 模数
     */
    private String modulus;
    /**
     * 指数
     */
    private String exponent;

    public RSA(String modulus, String exponent) {
        this.modulus = modulus;
        this.exponent = exponent;
    }

    public String getModulus() {
        return modulus;
    }

    public void setModulus(String modulus) {
        this.modulus = modulus;
    }

    public String getExponent() {
        return exponent;
    }

    public void setExponent(String exponent) {
        this.exponent = exponent;
    }
}

