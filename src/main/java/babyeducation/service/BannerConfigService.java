package babyeducation.service;

import babyeducation.model.BannerConfig;

import java.util.List;

public interface BannerConfigService extends BaseService<BannerConfig> {

    List<BannerConfig> selectBannerConfig();
}
