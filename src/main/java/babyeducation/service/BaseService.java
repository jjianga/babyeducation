
package babyeducation.service;


import java.util.List;

public interface BaseService<T> {

    /**
     * 查询所有
     *
     * @return
     * @throws Exception
     */
    List<T> selectAll() throws Exception;


    /**
     * 按条件查询记录数
     *
     * @param e
     * @return
     * @throws Exception
     */
    int countByExample(Object e) throws Exception;

    /**
     * 主键查询
     *
     * @param k
     * @return
     * @throws Exception
     */
    T selectByPrimaryKey(Integer k) throws Exception;

    /**
     * 按条件查询
     *
     * @param e
     * @return
     * @throws Exception
     */
    List<T> selectByExample(Object e) throws Exception;

    /**
     * 插入
     *
     * @param t
     * @return
     * @throws Exception
     */
    int insert(T t) throws Exception;

    /**
     * 插入（忽略空字段）
     *
     * @param t
     * @return
     * @throws Exception
     */
    int insertSelective(T t) throws Exception;

    /**
     * 批量插入
     *
     * @param list
     * @return
     * @throws Exception
     */


    int insertList(List<T> list) throws Exception;


    /**
     * 根据主键删除
     *
     * @param k
     * @throws Exception
     */
    void deleteByPrimaryKey(Integer k) throws Exception;

    /**
     * 根据条件删除
     *
     * @param e
     * @throws Exception
     */
    int deleteByExample(Object e) throws Exception;

    /**
     * 根据条件更新
     *
     * @param t
     * @param e
     * @throws Exception
     */
    int updateByExampleSelective(T t, Object e) throws Exception;

    /**
     * 根据条件更新
     *
     * @param t
     * @param e
     * @throws Exception
     */
    void updateByExample(T t, Object e) throws Exception;

    /**
     * 根据主键更新
     *
     * @param t
     * @throws Exception
     */
    void updateByPrimaryKeySelective(T t) throws Exception;

    /**
     * 根据主键更新
     *
     * @param t
     * @throws Exception
     */
    void updateByPrimaryKey(T t) throws Exception;

}
