package babyeducation.service;

import babyeducation.model.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookService extends BaseService<Book> {

    List<Book> selectBookForHomePage(String bookName,
                                     Integer gardenId,
                                     Integer pressId,
                                     Integer subjectId,
                                     Integer volume,Integer hasVideo,
                                     Integer hasBook) throws Exception;

    void deleteBook(Integer id) throws Exception;


    Book selectBookByBookId(Integer bookId) throws Exception;

    int addReadNumByBookId(Integer bookId) throws Exception;

    Book selectBookByVideoId(Integer videoId) throws Exception;


    List<Book> newSelectBookBase(Integer gardenId,
                                 Integer pressId,
                                 Integer subjectId,
                                 Integer volume,Integer hasVideo,
                                 Integer hasBook) throws Exception;
}
