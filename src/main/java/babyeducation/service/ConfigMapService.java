package babyeducation.service;

import babyeducation.model.ConfigMap;
import babyeducation.service.serviceImpl.BaseServiceImpl;

import java.util.List;
import java.util.Map;

public interface ConfigMapService extends BaseService<ConfigMap> {
    List<ConfigMap> selectConfigMap() throws Exception;

    Map<String,Object> selectConfigMapForLogin() throws Exception;
}
