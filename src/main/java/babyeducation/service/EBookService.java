package babyeducation.service;

import babyeducation.model.EBook;

import java.util.List;

public interface EBookService extends BaseService<EBook> {

    List<EBook> selectEBookByParameter(Integer gardenId, Integer subjectId, Integer EType) throws Exception;
}
