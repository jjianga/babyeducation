package babyeducation.service;

import babyeducation.model.Garden;

import java.util.List;

public interface GardenService extends BaseService<Garden> {

    void deleteGardenById(Integer id) throws Exception;

    List<Garden> selectGarden()throws Exception;

}
