package babyeducation.service;

import babyeducation.model.Manager;

import java.util.List;

/**
 * ManagerService
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public interface ManagerService extends BaseService<Manager> {
    /**
     * 查询所有
     *
     * @return
     */
    List<Manager> searchAll();

    /**
     * 添加管理员
     *
     * @param manager
     * @return 作者 谢唯依
     */
    int addManger(Manager manager);

    /**
     * 删除管理
     *
     * @param id
     * @return 作者 谢唯依
     */
    int deleteManger(Integer[] id);

    /**
     * 修改管理员信息
     *
     * @param manager 作者 谢唯依
     */
    int updateMangerInfo(Manager manager);

    /**
     * 查询管理员信息
     *
     * @param id
     * @param name
     * @param isSuper
     * @return
     */
    List<Manager> searchManager(Integer id, String name, String mob
            , Integer isSuper);

    /**
     * 修改密码 用于短信修改密码
     *
     * @param id
     * @param pwd
     * @return
     */
    int changePwd(Integer id, String pwd);

    /**
     * 修改密码 用于知道原始密码修改密码
     *
     * @param id
     * @param oldPwd
     * @param confirmPwd
     * @return
     */
    int updatePwd(Integer id, String oldPwd, String confirmPwd);

}
