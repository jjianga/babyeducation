package babyeducation.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * MyUserDetailsService
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public interface MyUserDetailsService extends UserDetailsService {
}
