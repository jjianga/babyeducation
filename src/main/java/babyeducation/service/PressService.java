package babyeducation.service;

import babyeducation.model.Press;

import java.util.List;

public interface PressService extends BaseService<Press> {

    void deletePressById(Integer id) throws Exception;

    List<Press> selectPress() throws Exception;
}
