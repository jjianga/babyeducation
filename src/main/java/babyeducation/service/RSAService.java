package babyeducation.service;


import babyeducation.model.RsaInfo;
import babyeducation.selfModel.RSA;

import java.util.List;

public interface RSAService extends BaseService<RsaInfo> {
    /**
     * 模数指数版获取公钥
     *
     * @param userInfo
     * @return
     */
    RSA getPublicKey(String userInfo);

    /**
     * 获取公钥
     *
     * @param userInfo
     * @return
     */
    String publicKey(String userInfo);

    /**
     * 获取密码
     *
     * @param userInfo
     * @param password
     * @return
     * @throws Exception
     */
    String getPwd(String userInfo, String password) throws Exception;

    /**
     * 模数指数版获取密码
     *
     * @param userInfo
     * @param password
     * @return
     * @throws Exception
     */
    String getPwd2(String userInfo, String password) throws Exception;


    int removeRSA(String userInfo);


    List<RsaInfo> selectPublicKey(String userInfo, String start_time, String end_time);


}
