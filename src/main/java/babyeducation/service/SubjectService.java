package babyeducation.service;

import babyeducation.model.Subject;

import java.util.List;

public interface SubjectService extends BaseService<Subject> {

    void deleteSubjectById(Integer id) throws Exception;

    List<Subject> selectSubject() throws Exception;
}
