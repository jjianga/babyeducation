package babyeducation.service;

import babyeducation.model.UserBookLog;

import java.sql.Timestamp;
import java.util.List;

public interface UserBookLogService extends BaseService<UserBookLog> {

    void updateCreateTimeById(Integer userId,Integer bookId) throws Exception;

    List<UserBookLog> selectUserBookLogByUserId(Integer userId) throws Exception;


}
