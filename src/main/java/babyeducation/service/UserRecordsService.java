package babyeducation.service;

import babyeducation.model.Book;
import babyeducation.model.UserRecords;

import java.util.List;

public interface UserRecordsService extends BaseService<UserRecords> {

    List<Book> selectRecordsByUserId(Integer userId) throws Exception;


    List<UserRecords> isRecordsByUserId(Integer userId, Integer BookId) throws Exception;

    List<UserRecords> hasVideoByUserId(Integer userId,Integer videoId) throws Exception;

    void insetRecord(UserRecords userRecords) throws  Exception;

    int updateOrderType(Integer orderId) throws Exception;

    List<UserRecords> searchUserRecords(String mob) throws Exception;
}
