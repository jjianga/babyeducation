package babyeducation.service;

import babyeducation.model.User;

import java.util.Date;
import java.util.List;

public interface UserService extends BaseService<User> {

    List<User> selectUserByMob(String mob) throws Exception;

    List<User> userLogin(String mob) throws Exception;

    int updateUserInfo(Integer userId, String nickName,
                       String userUrl, String gender,
                       Integer nowGarden, Integer age) throws Exception;

    int updateUserVipInfo(Integer userId, Integer isVip, Date statTime,Date endTime) throws Exception;



    User searchUserId(Integer userId) throws Exception;
}
