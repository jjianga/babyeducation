package babyeducation.service;

import babyeducation.model.UserBookLog;
import babyeducation.model.UserVideoBookLog;

import java.util.List;

public interface UserVideoBookLogService extends BaseService<UserVideoBookLog> {


    void updateCreateTimeById(Integer userId,Integer bookId) throws Exception;

    List<UserVideoBookLog> selectUserVideoBookLogByUserId(Integer userId) throws Exception;
}
