package babyeducation.service;

import babyeducation.model.VersionInfo;

public interface VersionInfoService extends BaseService<VersionInfo> {
}
