package babyeducation.service;

import babyeducation.model.Video;

import java.util.List;

public interface VideoService extends BaseService<Video> {


    void deleteVideoById(Integer id) throws Exception;

    List<Video> selectVideoByVbuId(Integer bookId) throws Exception;

    Video selectVideoByVideoId(Integer videoId) throws Exception;
}
