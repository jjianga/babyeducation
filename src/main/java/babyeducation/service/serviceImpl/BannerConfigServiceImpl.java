package babyeducation.service.serviceImpl;

import babyeducation.dao.BannerConfigMapper;
import babyeducation.model.BannerConfig;
import babyeducation.service.BannerConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BannerConfigServiceImpl extends BaseServiceImpl<BannerConfig> implements BannerConfigService {

   @Autowired
    BannerConfigMapper bannerConfigMapper;

    @Override
    public List<BannerConfig> selectBannerConfig() {
        return bannerConfigMapper.selectBannerConfig();
    }
}
