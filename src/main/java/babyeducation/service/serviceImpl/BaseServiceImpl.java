
package babyeducation.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import babyeducation.db.BaseMapper;
import babyeducation.service.BaseService;

import java.util.List;

/**
 * 基础实现层，所有ctroller层都可引入此类
 *
 * @param <T>
 */
@Service
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    protected Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    protected BaseMapper<T> baseMapper;

    /**
     * 查询所有
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<T> selectAll() throws Exception {
        return baseMapper.selectAll();
    }

    /**
     * 插入单条记录
     *
     * @param t
     * @return
     * @throws Exception
     */
    @Override
    public int insert(T t) throws Exception {
        return baseMapper.insert(t);
    }

    /**
     * 插入单条记录
     *
     * @param t
     * @return
     * @throws Exception
     */
    @Override
    public int insertSelective(T t) throws Exception {
        return baseMapper.insertSelective(t);
    }

    /**
     * 插入多条记录
     *
     * @param list
     * @return
     * @throws Exception
     */
    @Override
    public int insertList(List<T> list) throws Exception {
        return baseMapper.insertList(list);
    }

    /**
     * 根据主键删除
     *
     * @param k
     * @throws Exception
     */
    @Override
    public void deleteByPrimaryKey(Integer k) throws Exception {
        baseMapper.deleteByPrimaryKey(k);
    }

    /**
     * 根据条件删除
     *
     * @param e
     * @return
     * @throws Exception
     */
    @Override
    public int deleteByExample(Object e) throws Exception {
        return baseMapper.deleteByExample(e);
    }

    /**
     * 根据条件更新
     *
     * @param t
     * @throws Exception
     */
    @Override
    public void updateByPrimaryKeySelective(T t) throws Exception {
        baseMapper.updateByPrimaryKeySelective(t);
    }

    /**
     * 根据主键更新
     *
     * @param t
     * @throws Exception
     */
    @Override
    public void updateByPrimaryKey(T t) throws Exception {
        baseMapper.updateByPrimaryKey(t);
    }

    /**
     * 根据主键更新
     *
     * @param t
     * @param e
     * @return
     * @throws Exception
     */
    @Override
    public int updateByExampleSelective(T t, Object e) throws Exception {
        return baseMapper.updateByExampleSelective(t, e);
    }

    /**
     * 根据主键更新
     *
     * @param t
     * @param e
     * @throws Exception
     */
    @Override
    public void updateByExample(T t, Object e) throws Exception {
        baseMapper.updateByExample(t, e);
    }

    /**
     * 主键查询
     *
     * @param k
     * @return
     * @throws Exception
     */
    @Override
    public T selectByPrimaryKey(Integer k) throws Exception {
        return baseMapper.selectByPrimaryKey(k);
    }

    /**
     * 按条件查询
     *
     * @param e
     * @return
     * @throws Exception
     */
    @Override
    public List<T> selectByExample(Object e) throws Exception {
        return baseMapper.selectByExample(e);
    }

    /**
     * 按条件查询记录数
     *
     * @param e
     * @return
     * @throws Exception
     */
    @Override
    public int countByExample(Object e) throws Exception {
        return baseMapper.selectCountByExample(e);
    }

}

