package babyeducation.service.serviceImpl;

import babyeducation.dao.BookMapper;
import babyeducation.model.Book;
import babyeducation.service.BookService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl extends BaseServiceImpl<Book> implements BookService {

    @Autowired
    BookMapper bookMapper;


    @Override
    public List<Book> selectBookForHomePage(String bookName,
                                            Integer gardenId,
                                            Integer pressId,
                                            Integer subjectId,
                                            Integer volume,Integer hasVideo,
                                            Integer hasBook) {
        List<Book> bookList = bookMapper.selectBookForHomePage(gardenId, pressId, subjectId, volume, bookName, hasVideo, hasBook);

        return bookList;


    }

    @Override
    public void deleteBook(Integer id) throws Exception {
        bookMapper.deleteBookById(id);
    }

    @Override
    public Book selectBookByBookId(Integer bookId) throws Exception {
        return bookMapper.selectBookById(bookId);
    }

    @Override
    public int addReadNumByBookId(Integer bookId) throws Exception {
        return bookMapper.addReadNumByBookId(bookId);
    }

    @Override
    public Book selectBookByVideoId(Integer videoId) throws Exception {
        return bookMapper.selectBookInfoByVideoId(videoId);
    }

    @Override
    public List<Book> newSelectBookBase(Integer gardenId, Integer pressId, Integer subjectId, Integer volume, Integer hasVideo, Integer hasBook) throws Exception {
        List<Book> bookList = bookMapper.newSelectBookBase(gardenId, pressId, subjectId, volume , hasVideo, hasBook);



        return bookList;
    }
}
