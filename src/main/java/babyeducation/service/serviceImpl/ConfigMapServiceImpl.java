package babyeducation.service.serviceImpl;

import babyeducation.dao.ConfigMapMapper;
import babyeducation.model.ConfigMap;
import babyeducation.service.ConfigMapService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConfigMapServiceImpl extends BaseServiceImpl<ConfigMap> implements ConfigMapService {
    @Autowired
    ConfigMapMapper configMapMapper;


    @Override
    public List<ConfigMap> selectConfigMap() throws Exception {
        return configMapMapper.selectConfigMap();
    }

    @Override
    public Map<String, Object> selectConfigMapForLogin() throws Exception {

        List<ConfigMap> configMapList = configMapMapper.selectConfigMap();

        Map<String, Object> map = new HashMap<>();

        configMapList.forEach((ConfigMap a) -> {
                    try {
                        JSONArray jsonObject1 = JSONObject.parseArray(a.getValue());
                        map.put(a.getKey(), jsonObject1);
                    } catch (Exception e) {
                        map.put(a.getKey(), a.getValue());
                    }
                }
        );

//        Date stateDate = new Date();
//        for (int i = 0; i < configMapList.size(); i++) {
//            String key = configMapList.get(i).getKey();
//            String value = configMapList.get(i).getValue();
//            try {
//                JSONArray jsonObject1 = JSONObject.parseArray(value);
//                map.put(key, jsonObject1);
//            } catch (Exception e) {
//                map.put(key, value);
//            }
//
//        }
//        Date endDate = new Date();
//        long a = stateDate.getTime()-endDate.getTime();
//        System.out.println(a);
        return map;


    }
}
