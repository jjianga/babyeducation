package babyeducation.service.serviceImpl;

import babyeducation.dao.EBookMapper;
import babyeducation.model.EBook;
import babyeducation.service.EBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EBookServiceImpl extends BaseServiceImpl<EBook> implements EBookService {

    @Autowired
    EBookMapper eBookMapper;

    @Override
    public List<EBook> selectEBookByParameter(Integer gardenId, Integer subjectId, Integer eType) throws Exception {

        if (null != eType) {
            return eBookMapper.selectEBookByParameter(gardenId, subjectId, eType);
        } else {
            return eBookMapper.selectEBookNoEType(gardenId, subjectId);
        }


    }
}
