package babyeducation.service.serviceImpl;

import babyeducation.dao.GardenMapper;
import babyeducation.model.Garden;
import babyeducation.service.GardenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GardenServiceImpl extends BaseServiceImpl<Garden> implements GardenService {

    @Autowired
    GardenMapper gardenMapper;

    @Override
    public void deleteGardenById(Integer id) throws Exception {

    }

    @Override
    public List<Garden> selectGarden() throws Exception {
        return gardenMapper.selectGarden();
    }
}
