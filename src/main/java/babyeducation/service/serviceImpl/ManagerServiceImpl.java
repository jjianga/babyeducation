package babyeducation.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import babyeducation.dao.ManagerMapper;
import babyeducation.model.Manager;
import babyeducation.service.ManagerService;
import babyeducation.utils.MD5Util;
import babyeducation.utils.Utils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * ManagerServiceImpl
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
@Service
public class ManagerServiceImpl extends BaseServiceImpl<Manager> implements ManagerService {

    @Autowired
    ManagerMapper managerMapper;

    @Override
    public List<Manager> searchAll() {
        return managerMapper.selectAll();
    }

    @Override
    public int addManger(Manager manager) {
        Example example = new Example(Manager.class);
        //把传入的条件手机号mob 和state大于0的条件设置好
        example.or().andEqualTo("mob", manager.getMob()).andGreaterThanOrEqualTo("state", 1);
        //把条件带入去查询
        List<Manager> list = managerMapper.selectByExample(example);
        //如果数据不为空成立 说明查询到内容 返回-1 如果收到返回-1 说明tel电话已经备注测，不能添加
        if (!list.isEmpty()) {
            return -1;
        }
        //拿到密码
        String pwd = manager.getPwd();
        //进行MD5加密
        String Md5pwd = MD5Util.MD5(pwd);
        //塞回model
        manager.setPwd(Md5pwd);
        //根据条件 插入mysql数据库
        return managerMapper.insertSelective(manager);
    }

    @Override
    public int deleteManger(Integer[] id) {
        int res = 0;
        for (Integer i : id) {
            res += 1;
            managerMapper.updateManager(i);
        }
        return res;
    }

    @Override
    public int updateMangerInfo(Manager manager) {

        Example example = new Example(Manager.class);
        if (!Utils.checkNull(manager.getMob())) {
            if (!Utils.isMobileNO(manager.getMob())) {
                return -2;
            }
            example.or().andEqualTo("mob", manager.getMob()).andGreaterThanOrEqualTo("state", 0);
            List<Manager> list = managerMapper.selectByExample(example);
            if (!list.isEmpty()) {
                return -1;
            }
            return managerMapper.updateByPrimaryKeySelective(manager);
        }
        return managerMapper.updateByPrimaryKeySelective(manager);
    }

    @Override
    public List<Manager> searchManager(Integer id, String name, String mob,
                                       Integer isSuper) {


        return managerMapper.searchManager(id, name, mob, isSuper);

    }

    @Override
    public int updatePwd(Integer id, String oldPwd, String confirmPwd) {
        Example example = new Example(Manager.class);
        example.or().andEqualTo("id", id).andGreaterThanOrEqualTo("state", 0);
        List<Manager> list = managerMapper.selectByExample(example);
        if (list.isEmpty()) {
            return -1;
        }
        if (oldPwd.equals(list.get(0).getPwd())) {
            Manager manager = new Manager();
            manager.setId(id);
            manager.setPwd(confirmPwd);
            return managerMapper.updateByPrimaryKeySelective(manager);
        }
        return -2;
    }

    @Override
    public int changePwd(Integer id, String pwd) {
        Manager manager = new Manager();
        //进行MD5加密塞回model
        String Md5pwd = MD5Util.MD5(pwd);
        manager.setPwd(Md5pwd);
        manager.setId(id);
        return managerMapper.updateByPrimaryKeySelective(manager);
    }
}
