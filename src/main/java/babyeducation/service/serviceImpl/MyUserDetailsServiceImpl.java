package babyeducation.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import babyeducation.dao.ManagerMapper;
import babyeducation.model.Manager;
import babyeducation.selfModel.CurrentUser;
import babyeducation.service.MyUserDetailsService;
import babyeducation.utils.LoginException;
import babyeducation.utils.Utils;
import tk.mybatis.mapper.entity.Example;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * MyUserDetailsServiceImpl
 *
 * @author xieweyi
 * @date 2019/5/28
 */
@Service
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

    final Logger logger = LoggerFactory.getLogger(MyUserDetailsServiceImpl.class);
    @Autowired
    ManagerMapper managerMapper;

    @Override
    public UserDetails loadUserByUsername(String userInfo) throws UsernameNotFoundException {
        if (Utils.checkNull(userInfo)) {
            logger.warn("用户名为空");
            throw new LoginException("login.error.noIdCard");
        }
        //GrantedAuthority 身份列表
        Set<GrantedAuthority> authorities = new HashSet<>();
        String actor = userInfo.split("_")[0];
        String info = userInfo.split("_")[1];
        logger.info("actor: {}, info: {}", actor, info);
        CurrentUser currentUser = null;
        switch (actor) {
            case "Manager":
                Example example = new Example(Manager.class);
                example.or()
                        .andEqualTo("mob", info)
                        .andGreaterThanOrEqualTo("state", 0);
                List<Manager> managers = managerMapper.selectByExample(example);
                if (null != managers && managers.size() > 0) {
                    Manager nowManager = managers.get(0);
                    if (1 != nowManager.getState()) {
                        logger.warn("{} 用户状态异常", info);
                        throw new LoginException("login.error.noState");
                    }
                    currentUser = new CurrentUser("Manager_" + nowManager.getMob(), nowManager.getPwd(), authorities);
                    currentUser.setUsers(nowManager);
                } else {
                    logger.error("info：{} 不存在", info);
                    throw new LoginException("login.error.noIdCard");
                }
                break;
            default:
                logger.error("actor：{} 不存在", actor);
                throw new LoginException("login.error.noIdCard");

        }
        return currentUser;
    }
}
