package babyeducation.service.serviceImpl;

import babyeducation.dao.PressMapper;
import babyeducation.model.Press;
import babyeducation.service.PressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PressServiceImpl extends BaseServiceImpl<Press> implements PressService {

    @Autowired
    PressMapper pressMapper;

    @Override
    public void deletePressById(Integer id) throws Exception {

        pressMapper.deletePressById(id);

    }

    @Override
    public List<Press> selectPress() throws Exception {
        return pressMapper.selectPress();
    }
}
