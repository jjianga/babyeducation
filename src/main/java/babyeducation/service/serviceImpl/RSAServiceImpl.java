package babyeducation.service.serviceImpl;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import babyeducation.dao.RsaInfoMapper;
import babyeducation.model.RsaInfo;
import babyeducation.selfModel.RSA;
import babyeducation.service.RSAService;
import babyeducation.utils.RSAUtil;
import tk.mybatis.mapper.entity.Example;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;

@Service
public class RSAServiceImpl extends BaseServiceImpl<RsaInfo> implements RSAService {
    @Autowired
    RsaInfoMapper rsaInfoMapper;


    @Override
    public RSA getPublicKey(String userInfo) {

        Example example = new Example(RsaInfo.class);

        example.or().andEqualTo("userInfo").andGreaterThanOrEqualTo("state", 0);
        //查询是否已经包含公钥
        List<RsaInfo> rsaInfos = rsaInfoMapper.selectByExample(example);

        try {
            if (null == rsaInfos || rsaInfos.isEmpty()) {

                KeyPair keyPair = RSAUtil.getKeyPair();
                //获取公钥
                PublicKey publicKey = keyPair.getPublic();
                //因为公钥 不是字符串，通过base64位转码成字符串格式的对象
                String publickKeyStr = new String(Base64.encodeBase64(publicKey.getEncoded()));
                //获取私钥
                PrivateKey privateKey = keyPair.getPrivate();
                //转字符串
                String privateKeyStr = new String(Base64.encodeBase64(privateKey.getEncoded()));

                RsaInfo rsaInfo = new RsaInfo();
                rsaInfo.setUserInfo(userInfo);
                rsaInfo.setPublicKey(publickKeyStr);
                rsaInfo.setPrivateKey(privateKeyStr);
                //插入数据库
                int res = rsaInfoMapper.insertSelective(rsaInfo);
                if (1 == res) {
                    return getRSA(publickKeyStr);
                } else {
                    return getRSA(rsaInfos.get(0).getPublicKey());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    public String publicKey(String userInfo) {
        Example example = new Example(RsaInfo.class);
        example.or().andEqualTo("userInfo").andGreaterThanOrEqualTo("state", 0);
        List<RsaInfo> rsaInfos = rsaInfoMapper.selectByExample(example);
        try {
            if (null == rsaInfos || rsaInfos.isEmpty()) {

                KeyPair keyPair = RSAUtil.getKeyPair();
                //获取公钥
                PublicKey publicKey = keyPair.getPublic();
                //因为公钥 不是字符串，通过base64位转码成字符串格式的对象
                String publickKeyStr = new String(Base64.encodeBase64(publicKey.getEncoded()));
                //获取私钥
                PrivateKey privateKey = keyPair.getPrivate();
                //转字符串
                String privateKeyStr = new String(Base64.encodeBase64(privateKey.getEncoded()));
                RsaInfo rsaInfo = new RsaInfo();
                rsaInfo.setUserInfo(userInfo);
                rsaInfo.setPublicKey(publickKeyStr);
                rsaInfo.setPrivateKey(privateKeyStr);
                //插入数据库
                int res = rsaInfoMapper.insertSelective(rsaInfo);
                if (1 == res) {
                    return publickKeyStr;
                }
            } else {
                RsaInfo rsaInfo = rsaInfos.get(0);
                return rsaInfo.getPublicKey();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    public String getPwd(String userInfo, String password) throws Exception {
        Example example = new Example(RsaInfo.class);
        example.or().andEqualTo("userInfo").andGreaterThanOrEqualTo("state", 0);
        List<RsaInfo> rsaInfos = rsaInfoMapper.selectByExample(example);
        if (rsaInfos.size() > 0) {
            //从数据库获取私钥
            String privaterKeyStr = rsaInfos.get(0).getPrivateKey();
            PrivateKey privateKey = RSAUtil.getPrivateKey(privaterKeyStr);
            //base64反转码
            return new String(RSAUtil.decrypt(Base64.decodeBase64(password), privateKey));
        }
        throw new Exception("数据为空");
    }

    @Override
    public String getPwd2(String userInfo, String password) throws Exception {
        Example example = new Example(RsaInfo.class);
        example.or().andEqualTo("userInfo").andGreaterThanOrEqualTo("state", 0);
        List<RsaInfo> rsaInfos = rsaInfoMapper.selectByExample(example);
        if (rsaInfos.size() > 0) {
            //从数据库获取私钥
            String privaterKeyStr = rsaInfos.get(0).getPrivateKey();
            PrivateKey privateKey = RSAUtil.getPrivateKey(privaterKeyStr);
            //base64反转码
            return new String(
                    decryptPrivateKey(password, privaterKeyStr));
        }
        throw new Exception("数据为空");

    }

    @Override
    public int removeRSA(String userInfo) {
        Example example = new Example(RsaInfo.class);
        example.or().andEqualTo("userInfo", userInfo);
        return rsaInfoMapper.deleteByExample(example);
    }

    @Override
    public List<RsaInfo> selectPublicKey(String userInfo, String start_time, String end_time) {
        return rsaInfoMapper.selectPublicKey(userInfo, start_time, end_time);


    }

    /**
     * 拆解公钥 生成模数指数
     *
     * @param publicKeyStr
     * @return
     * @throws Exception
     */
    private RSA getRSA(String publicKeyStr) throws Exception {
        // 将公钥转化成RSA公钥
        RSAPublicKey rsaPublicKey = (RSAPublicKey) RSAUtil.getPublicKey(publicKeyStr);
        //获取模数
        String modulus = rsaPublicKey.getModulus().toString();
        //获取指数
        String exponent = rsaPublicKey.getPublicExponent().toString();
        return new RSA(modulus, exponent);
    }

    /**
     * 私钥解密
     *
     * @param pwd
     * @param privateKeyStr
     * @return
     * @throws Exception
     */
    private byte[] decryptPrivateKey(String pwd, String privateKeyStr) throws Exception {
        //将私钥转化为RSA私钥
        RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey) RSAUtil.getPrivateKey(privateKeyStr);
        //获取模数
        String modulus = rsaPrivateCrtKey.getModulus().toString();
        //获取指数
        String exponent = rsaPrivateCrtKey.getPrivateExponent().toString();
        //利用模数指数生产钥匙
        PrivateKey privateKey = RSAUtil.getPrivateKey(modulus, exponent);
        //解密
        return RSAUtil.decrypt(Base64.decodeBase64(pwd), privateKey);

    }
}
