package babyeducation.service.serviceImpl;

import babyeducation.dao.SubjectMapper;
import babyeducation.model.Subject;
import babyeducation.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectServiceImpl extends BaseServiceImpl<Subject> implements SubjectService {

    @Autowired
    SubjectMapper subjectMapper;

    @Override
    public void deleteSubjectById(Integer id) throws Exception {
        subjectMapper.deleteSubjectById(id);
    }

    @Override
    public List<Subject> selectSubject() throws Exception {
        return subjectMapper.selectSubject();
    }
}
