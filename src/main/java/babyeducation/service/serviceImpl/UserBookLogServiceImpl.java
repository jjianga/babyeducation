package babyeducation.service.serviceImpl;

import babyeducation.dao.UserBookLogMapper;
import babyeducation.model.UserBookLog;
import babyeducation.service.UserBookLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class UserBookLogServiceImpl extends BaseServiceImpl<UserBookLog> implements UserBookLogService {

    @Autowired
    UserBookLogMapper userBookLogMapper;



    @Override
    public void updateCreateTimeById(Integer userId,Integer bookId) throws Exception {


        Example example = new Example(UserBookLog.class);
        example.or().andEqualTo("userId", userId)
                .andEqualTo("bookId", bookId);
        List<UserBookLog> userBookLogList = userBookLogMapper.selectByExample(example);
        if (0 != userBookLogList.size()) {
            java.util.Date date = new Date();
            Timestamp ctime = new java.sql.Timestamp(date.getTime());
            userBookLogMapper.updateCreateTimeById(ctime, userBookLogList.get(0).getId());
        } else {
            UserBookLog userBookLog = new UserBookLog();
            userBookLog.setUserId(userId);
            userBookLog.setBookId(bookId);
            userBookLogMapper.insertSelective(userBookLog);
        }

    }

    @Override
    public List<UserBookLog> selectUserBookLogByUserId(Integer userId) throws Exception {
        return userBookLogMapper.selectUserBookLogByUserId(userId);
    }
}
