package babyeducation.service.serviceImpl;

import babyeducation.dao.BookMapper;
import babyeducation.dao.UserRecordsMapper;
import babyeducation.model.Book;
import babyeducation.model.UserRecords;
import babyeducation.service.UserRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRecordsServiceImpl extends BaseServiceImpl<UserRecords> implements UserRecordsService {

    @Autowired
    UserRecordsMapper userRecordsMapper;
    @Autowired
    BookMapper bookMapper;

    @Override
    public List<Book> selectRecordsByUserId(Integer userId) throws Exception {
        return bookMapper.selectUserRecordsById(userId);
    }

    @Override
    public List<UserRecords> isRecordsByUserId(Integer userId, Integer BookId) throws Exception {
        return userRecordsMapper.isRecordsByUserId(userId, BookId);
    }

    @Override
    public List<UserRecords> hasVideoByUserId(Integer userId, Integer videoId) throws Exception {
        return userRecordsMapper.hasVideoByUserId(userId, videoId);
    }

    @Override
    public void insetRecord(UserRecords userRecords) throws Exception {

        userRecordsMapper.insetRecords(userRecords);


    }

    @Override
    public int updateOrderType(Integer orderId) throws Exception {


        return userRecordsMapper.updateOrderType(orderId);
    }

    @Override
    public List<UserRecords> searchUserRecords(String mob) throws Exception {

        List<UserRecords> userRecordsList = userRecordsMapper.selectUserRecords(mob);
        return userRecordsList;
    }
}
