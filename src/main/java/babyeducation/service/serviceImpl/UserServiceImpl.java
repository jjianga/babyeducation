package babyeducation.service.serviceImpl;

import babyeducation.dao.UserMapper;
import babyeducation.model.User;
import babyeducation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public List<User> selectUserByMob(String mob) throws Exception {
        return userMapper.selectUserByMob(mob);
    }

    @Override
    public List<User> userLogin(String mob) throws Exception {
        List<User> userList = userMapper.selectUserByMob(mob);
        if (0 != userList.size()) {
            User user = new User();
            user = userList.get(0);
            int isVip = user.getIsVip();
            if (1 == isVip) {
                Date date = user.getEndTime();
                Date nowDate = new Date();
                boolean isBefore = nowDate.after(date);
                if (isBefore) {
                    user.setIsVip(0);
                    userMapper.clearVip(user.getUserId());
                    userList.set(0, user);
                    return userList;
                }
            }
        }
        return userList;
    }

    @Override
    public int updateUserInfo(Integer userId, String nickName, String userUrl, String gender, Integer nowGarden, Integer age) throws Exception {
        return userMapper.updateUserInfo(userId,nickName,userUrl,gender,nowGarden,age);
    }

    @Override
    public int updateUserVipInfo(Integer userId, Integer isVip, Date statTime, Date endTime) throws Exception {
        return userMapper.updateUserVipInfo(isVip,userId,statTime,endTime);
    }



    @Override
    public User searchUserId(Integer userId) throws Exception {

        User userInfo = userMapper.searchUserId(userId);
        if (null==userInfo){
            userInfo  = new User();
            userInfo.setNickName("游客");
            Date date = new Date();
            long mob = date.getTime();
            String mobs = String.valueOf(mob);
            userInfo.setMob(mobs);

            userMapper.newUserBy(userInfo);
            userInfo=userMapper.searchUserId(userInfo.getUserId());
        }


        return userInfo;
    }
}
