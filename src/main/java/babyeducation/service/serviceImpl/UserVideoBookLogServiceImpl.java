package babyeducation.service.serviceImpl;

import babyeducation.dao.UserVideoBookLogMapper;
import babyeducation.model.UserBookLog;
import babyeducation.model.UserVideoBookLog;
import babyeducation.service.UserVideoBookLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class UserVideoBookLogServiceImpl extends BaseServiceImpl<UserVideoBookLog> implements UserVideoBookLogService {
    @Autowired
    UserVideoBookLogMapper userVideoBookLogMapper;


    @Override
    public void updateCreateTimeById(Integer userId, Integer bookId) throws Exception {
        Example example = new Example(UserBookLog.class);
        example.or().andEqualTo("userId", userId)
                .andEqualTo("bookId", bookId);
        List<UserVideoBookLog> userVideoBookLogList = userVideoBookLogMapper.selectByExample(example);
        if (0 != userVideoBookLogList.size()) {
            java.util.Date date = new Date();
            Timestamp ctime = new java.sql.Timestamp(date.getTime());
            userVideoBookLogMapper.updateCreateTimeById(ctime, userVideoBookLogList.get(0).getId());
        } else {
            UserVideoBookLog userVideoBookLog = new UserVideoBookLog();
            userVideoBookLog.setUserId(userId);
            userVideoBookLog.setBookId(bookId);
            userVideoBookLogMapper.insertSelective(userVideoBookLog);
        }
    }

    @Override
    public List<UserVideoBookLog> selectUserVideoBookLogByUserId(Integer userId) throws Exception {
        return userVideoBookLogMapper.selectUserVideoBookLogByUserId(userId);
    }
}
