package babyeducation.service.serviceImpl;

import babyeducation.model.VersionInfo;
import babyeducation.service.VersionInfoService;
import org.springframework.stereotype.Service;

@Service
public class VersionInfoServiceImpl extends BaseServiceImpl<VersionInfo> implements VersionInfoService {
}
