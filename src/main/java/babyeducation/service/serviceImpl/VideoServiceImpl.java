package babyeducation.service.serviceImpl;

import babyeducation.dao.VideoMapper;
import babyeducation.model.Video;
import babyeducation.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl extends BaseServiceImpl<Video> implements VideoService {

    @Autowired
    VideoMapper videoMapper;

    @Override
    public void deleteVideoById(Integer id) throws Exception {
            videoMapper.deleteVideoById(id);
    }

    @Override
    public List<Video> selectVideoByVbuId(Integer bookId) throws Exception {
        return videoMapper.selectVideoByVbuId(bookId);
    }

    @Override
    public Video selectVideoByVideoId(Integer videoId) throws Exception {
        return videoMapper.selectVideoByVideoId(videoId);
    }
}
