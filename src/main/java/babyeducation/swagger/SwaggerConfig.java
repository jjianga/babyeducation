package babyeducation.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2//启用Swagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        ParameterBuilder token = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        token.name("x-auth-token").description("token") //请求Key名 注释
                .modelRef(new ModelRef("String")) //数据类型
                .parameterType("header") //类型
                .required(false) //是否必填
                .build();
        pars.add(token.build());
        token.name("token_id").description("token") //请求Key名 注释
                .modelRef(new ModelRef("String")) //数据类型
                .parameterType("header") //类型
                .required(false) //是否必填
                .build();
        pars.add(token.build());

        //先初始化一个Docket对象 设置该对象为Swagger2
        //配置apiInfo 创建该Api的基本信息（这些基本信息会展现在文档页面中）
        //select() 函数返回一个ApiSelectorBuilder实例用来控制哪些接口暴露给Swagger来展现
        //指定basePackage的位置 让文档从这个包开始扫描
        //paths 设置路劲扫描规则
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("babyeducation"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars);
    }

    /**
     * 配置ui页面的显示信息  例如标题 版本等
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("宝宝教育")
                .description("我是详情")
                .version("1.0.0")
                .build();
    }

}
