package babyeducation.utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;

public class AlbabaSendMsg {

    public static HashMap<String, String> registerMessageStatus(String tel, Integer type) throws Exception {
        String mode = null;
        if (1 == type) {
            mode = "SMS_175535809";
        } else if (2 == type) {
            mode = "SMS_175570794";
        }

        HashMap<String, String> m = new HashMap<String, String>();
        String code = VerifyingCodeGenerator.generate();//获得验证码
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIVEZwJARjuuBm", "sGqqUAfVDDSgEIbUCdBVwNE7frdixk");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", tel);
        request.putQueryParameter("SignName", "Yonge工作室");

        request.putQueryParameter("TemplateCode", mode);
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");

        CommonResponse response = client.getCommonResponse(request);
        m.put("code", code);
        m.put("result", response.getData());

        return m;
    }
}
