package babyeducation.utils;


import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

public class CompareStringArrayUtil {
    /**
     * @param oldArr 旧数组
     * @param newArr 新数组
     * @return Map 包含两个数组，相较于旧数组，新数组多了哪些元素，以及少了哪些元素
     */
    public static Map<String, String[]> CompareStringArray(String[] oldArr, String[] newArr) {
        List<String> addList = new ArrayList<>();
        List<String> deleteList = new ArrayList<>();

        for (String anOldArr : oldArr) {
            if (!ArrayUtils.contains(newArr, anOldArr)) {
                deleteList.add(anOldArr);
            }
        }

        for (String aNewArr : newArr) {
            if (!ArrayUtils.contains(oldArr, aNewArr)) {
                addList.add(aNewArr);
            }
        }

        String[] addArr = addList.toArray(new String[addList.size()]);
        String[] deleteArr = deleteList.toArray(new String[deleteList.size()]);
        Map<String, String[]> res = new HashMap<>();
        res.put("addArr", addArr);
        res.put("deleteArr", deleteArr);
        return res;
    }

    public static void main(String[] args) {
        String labels = "可爱,哈哈";
        String[] ids = labels.split(",");
        System.out.println(Arrays.toString(ids));
        String[] oldArr = {"可爱", "M诶个"};

        String[] newArr = {"1,2,3"};
        Map<String, String[]> res = CompareStringArrayUtil.CompareStringArray(ids, newArr);
        String[] deleteArr = res.get("deleteArr");
        String[] addArr = res.get("addArr");
        int a = addArr.length;
        System.out.println("deleteArr: " + Arrays.toString(res.get("deleteArr")));
        System.out.println("addArr: " + Arrays.toString(res.get("addArr")) + a);
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ;i<addArr.length;i++){
            sb.append(addArr[i]+",");

        }
        labels = labels+","+ sb;
        System.out.println(labels);
    }

}


