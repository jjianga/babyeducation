package babyeducation.utils;


import org.springframework.stereotype.Service;

/**
 * ConstantUtil
 *
 * @author wuzhongping
 * @date 2019/3/25
 */
@Service
public class ConstantUtil {
    public static final String GET_PUBLIC_FLIED = "获取公钥失败";
    public static final String GET_PUBLIC_SUCCEED = "获取公钥成功";
    public static final String THERE_ID_ON_RECORD_OF_THIS = "没有该条记录";
    public static final String THE_LACK_OF_THE_PARTICIPATION = "缺少入参";
    public static final String PARAMETERS_OF_THE_ABNORMAL = "参数异常";
    public static final String PRINT_ERROR = "输入数字错误";
    public static final String ADD_SUCCESS = "添加成功";
    public static final String ADD_FAIL = "添加失败";
    public static final String SELECT_SUCCESS = "查询成功";
    public static final String SELECT_FAIL = "查询失败";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DISABLE_SUCCESS = "禁用成功";
    public static final String DELETE_FAIL = "删除失败";
    public static final String DELETE_STATE = "该数据已是删除状态";
    public static final String UPDATE_SUCCESS = "修改成功";
    public static final String UPDATE_FAIL = "修改失败";
    public static final String NONENTIT = "数据不存在";
    public static final String DECRYPTION_FAIL = "解密失败";
    public static final String LOGIN_SUCCESS = "登录成功";
    public static final String LOGIN_FAIL = "登录失败";
    public static final String PASSWORD_CORRECT = "密码正确";
    public static final String PASSWORD_FAIL = "密码错误";
    public static final String IDENTITY_INFORMATION_INVALID = "身份信息失效";
    public static final String Data_Already_Exists = "数据已存在";
    public static final String INSUFFICIENT_AUTHORITY = "权限不足";
    public static final String ID_NOT_EXIST = "id不存在";
    public static final String SYSTEM_ERROR = "系统错误";
    public static final String THE_ABNORMAL_REFS = "入参异常";
    public static final String DISABLE_THE_SUCCESS = "禁用成功";
    public static final String THE_CAMERA_EXISTS = "摄像头不存在";
    public static final String THIS_CONFIGURATION_EXISTS = "存在此项配置";

    public static final String OPERATION_SUCCESS = "操作成功";
    public static final String OPERATION_FAILED = "操作失败";

    public static final String STOP_SUCCESS = "停止成功";
    public static final String STOP_FAILED = "停止失败";
}
