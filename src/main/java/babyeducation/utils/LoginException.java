package babyeducation.utils;

import org.springframework.security.core.AuthenticationException;

/**
 * LoginException
 * 登录异常
 *
 * @author xieweiyi
 * @date 2019/5/28
 */
public class LoginException extends AuthenticationException {
    public LoginException(String msg) {
        super(msg);
    }
}
