package babyeducation.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RepetitiveRandomArrays {

    public static List<Integer> getNonRepetitiveRandomArrays(int listSize, double advertisingProbability) {


        List<Integer> list = new ArrayList<>();
        int cont = 1;
        int tmp = (int) (Math.random() * listSize);
        int b = (int) (advertisingProbability * listSize);
        while (cont <= b) {
            if (list.contains(tmp)) {
                tmp = (int) (Math.random() * listSize);
            } else {
                list.add(tmp);
                cont++;
            }
        }
        Collections.sort(list);
        return list;
    }
}
