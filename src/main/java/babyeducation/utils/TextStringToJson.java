package babyeducation.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.tomcat.jni.Time;
import org.springframework.security.config.annotation.web.configurers.UrlAuthorizationConfigurer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TextStringToJson implements Runnable {
    private static final String FORMAT = "yyyy-MM-dd";
    private static final Integer time = 100;
    String url1 = "http://154.223.135.44:6700/babyeducation/api/user/login?mob=15111323354&pwd=123456";
    String url2 = "http://154.223.135.44:6700/babyeducation/api/user/login?mob=15005152355&pwd=242806";

    public static void main(String[] args) {

        Thread a = new Thread(new TextStringToJson());
        Thread b = new Thread(new TextStringToJson());
        Thread c = new Thread(new TextStringToJson());
        Thread d = new Thread(new TextStringToJson());
        Thread e = new Thread(new TextStringToJson());
        Thread f = new Thread(new TextStringToJson());
        Thread g = new Thread(new TextStringToJson());
        Thread h = new Thread(new TextStringToJson());
        Thread i = new Thread(new TextStringToJson());
        Thread j = new Thread(new TextStringToJson());
        Thread k = new Thread(new TextStringToJson());
        a.start();
        b.start();
        c.start();
        d.start();
        e.start();
        f.start();
        g.start();
        h.start();
        i.start();
        j.start();
        k.start();


    }

    public static String doGets(String httpUrl) {

        HttpURLConnection connection = null;
        InputStream is = null;
        BufferedReader br = null;
        String result = null;
        try {
            URL url = new URL(httpUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(60000);
            connection.connect();
            if (connection.getResponseCode() == 200) {
                is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuffer sbf = new StringBuffer();
                String temp = null;
                while ((temp = br.readLine()) != null) {

                    sbf.append(temp);
                    sbf.append("\r\n");
                }
                result = sbf.toString();

            }


        } catch (Exception e) {

            return e.getMessage();

        } finally {

            if (null != br) {
                try {

                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

        }
        return result;

    }

    @Override
    public void run() {
        for (int i = 0; i < time; i++) {
            Thread t = Thread.currentThread();

            Date date = new Date();
            doGets(url2);
            System.out.println(t.getName()+"第："+i+"次");
            Date endDate = new Date();
            long time = endDate.getTime() - date.getTime();
            System.out.println("耗时："+time+"毫秒");
        }
    }
}

