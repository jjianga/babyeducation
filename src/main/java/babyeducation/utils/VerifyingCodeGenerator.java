package babyeducation.utils;

import java.util.Random;

/**
 * 生成随机的6位数验证码方法 用于短信发送
 * 暂时还没对接短信平台
 */
public class VerifyingCodeGenerator {


    public static String generate() {
        String verifyCode = String.valueOf(new Random().nextInt(899999) + 100000);
        return verifyCode;
    }
}
