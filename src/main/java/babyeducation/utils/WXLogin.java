package babyeducation.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：liu jun
 * @date ：2019/6/3 16:14
 * @description：微信登录的工具类
 * @version: 1.0
 */
public class WXLogin {
    private static final String APPID = "wxfef1be5ae7cf66d6";
    private static final String APPSECRET = "52e8b23d2ef12e8b6cbfc4bfe0515960";
    private static final Logger LOG = LoggerFactory.getLogger(WXLogin.class);

    public static String getOpenId(String code) {
        return getOpenId(code, APPID, APPSECRET);
    }

    public static String getOpenId(String code, String appID, String  appSecret) {
        String body = null;
        //官方接口，需要自己提供appid，secret和js_code
        String requestUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appID + "&secret=" + appSecret + "&js_code=" + code + "&grant_type=authorization_code";
        try {
            RestTemplate restTemplate = new RestTemplate();
            body = restTemplate.getForEntity(requestUrl, String.class).getBody();
        } catch (Exception e) {
            LOG.error("get the WXURL error!");
        }
        return body;
    }

}
