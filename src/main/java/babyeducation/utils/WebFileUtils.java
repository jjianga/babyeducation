package babyeducation.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 *
 */
public class WebFileUtils {
    final static Logger log = LoggerFactory.getLogger(WebFileUtils.class);

    /**
     * 从网络Url中下载文件
     *
     * @param urlStr
     * @param fileDir 文件保存位置
     * @param name
     * @throws IOException
     */
    public static void downLoadFromUrl(String urlStr, File fileDir, String name, String toekn) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //设置超时间为3秒
        conn.setConnectTimeout(3 * 1000);
        //防止屏蔽程序抓取而返回403错误
        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
        conn.setRequestProperty("lfwywxqyh_token", toekn);
        //得到输入流
        InputStream inputStream = conn.getInputStream();
        //获取自己数组
        byte[] getData = readInputStream(inputStream);
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
        FileOutputStream fos = new FileOutputStream(new File(fileDir, name));
        fos.write(getData);
        if (fos != null) {
            fos.close();
        }
        if (inputStream != null) {
            inputStream.close();
        }
        log.info("info: {}download success", url);
    }

    /**
     * 从输入流中获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

    public static void writer(File file, String str) {
        Path fpath = file.toPath();
        //创建文件
        if (!Files.exists(fpath)) {
            try {
                file.getParentFile().mkdirs();
                Files.createFile(fpath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //创建BufferedWriter
        try {
            BufferedWriter bfw = Files.newBufferedWriter(fpath);
            bfw.write(str);
            bfw.flush();
            bfw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String reader(File file) {
        Path fpath = file.toPath();
        String str = null;
        //创建文件
        if (!Files.exists(fpath)) {
            try {
                Files.createFile(fpath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //创建BufferedReader
        try {
            BufferedReader bfr = Files.newBufferedReader(fpath);
            str = bfr.readLine();
            bfr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}

