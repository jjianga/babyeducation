//把一个文件加载出来变成对象 TEST/Wake.json
function classMapper(project) {
    var that = {};
    var server = "https://zcdn.zhihuangongshe.com/";
    that.project = project || "clazzMapper/";
    that.state = '未加载数据！';

    that.loadObject = function (path, className, success) {
        $.ajax({
            type: "GET", 	//请求方式 ("POST" 或 "GET")， 默认为 "GET"    
            url: server + that.project + (path ? '/' + path : '') + '/' + (className || 'default') + '.json?v=0' + new Date().getTime(),	//请求地址  
            success: function (data) {
                that.state = '数据加载成功！';
                if (data) {
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        console.log(data);
                        data = data;
                    }
                }
                console.log('load', typeof (data), data);
                success(data);
            },
            error: function (err) {
                that.state = '数据加载失败！';
                console.log('err', err);
                success(null);
            }
        });
    }

    that.setObject = function (path, className, clazz) {
        var string = JSON.stringify(clazz);
        console.log('上传：', string);
        aliyunOss.uploadingText(that.project + (path ? '/' + path : '') + '/' + (className || 'default') + '.json', string)
    }

    return that;
}
