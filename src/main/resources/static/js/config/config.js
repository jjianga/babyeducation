// 每页展示多少条数据
var PAGE_SIZE = 10
// 提示信息
var DELETE_INFO = "确认删除吗？"
var DELETE_SUCCESS_INFO = "删除成功！"
var ADD_SUCCESS_INFO = "新增成功！"
var UPDATE_SUCCESS_INFO = "修改成功！"
var HANDLE_SUCCESS_INFO = "操作成功！"
var TIME_RANGE_ERROR = "时间范围错误！"


var TEL_OR_PWD_IS_NULL = "手机号或密码不能为空！";
var GET_PUBLIC_KEY_FAILED = "获取公钥失败，请稍后再试！";

var NETWORK_ERROR = "网络开小差了,请稍后再试！";
var REQUEST_TIMEOUT = "请求超时，请稍后再试！";

var IDENTITY_FAILURE = "身份信息失效,请重新登录";


//提示框初始化，toast-top-center表示提示框的位置
toastr.options = {
    positionClass: 'toast-top-center', // 提示框位置
    closeButton: true  // 是否显示关闭按钮
};


function loginOut() {
    $.ajax({
        url: "../api/manager/loginOut",
        type: "POST",
        data: {},
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
        },
        error: function (e) {

        },
        complete: function (xhr, status) {
            // 完成时的回调
            if ("timeout" == status) {

            }
        }
    });
    // 跳页
    location.href = "../babyeducation/login"
}

