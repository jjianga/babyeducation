console.log($.cookie("token"));
$(document).ready(function () {
    // selectGardenConsultation();
    // searchSong();
    // selectVideoDetail();
    // selectUser();

});

function selectGardenConsultation() {
    $.ajax({
        url: "../api/feedback/selectFeedback",
        type: "GET",
        data: {},
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            if ("success" == data.flag) {
                var list = data.result.list;
                $("#feedback_num").html(list.length)
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            $("#loadingModal").modal("hide");
            // 完成时的回调

        }
    })

}

// 查询歌曲
function searchSong() {
    $.ajax({
        url: "../api/advertisement/selectAdvertisement",
        type: "GET",
        data: {},
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if ("success" == data.flag) {
                var str = '';
                var list = data.result.list;
                $("#advertisement_num").html(list.length)
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
        }
    })

}

// 视频
function selectVideoDetail() {
    $.ajax({
        url: "../api/video/selectVideo",
        type: "GET",
        data: {},
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if ("success" == data.flag) {
                var str = '';
                var list = data.result.list;
                $("#video_num").html(list.length)
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
        }
    })

}

// 查询用户
function selectUser() {
    $.ajax({
        url: "../api/user/selectUser",
        type: "GET",
        data: {},
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            if ("success" == data.flag) {
                var str = '';
                var list = data.result.list;
                $("#user_num").html(list.length)
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
        }
    })

}
