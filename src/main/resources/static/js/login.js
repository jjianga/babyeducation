//提示框初始化，toast-top-center表示提示框的位置
toastr.options = {
    positionClass: 'toast-top-center', // 提示框位置
    closeButton: true  // 是否显示关闭按钮
};

$(document).ready(function () {

    // 往cookie中存信息$.cookie(名称, 值) 值只能是字符串
    $.cookie("userInfo", "admin");
    // $.cookie("userInfo") 根据名称在cookie中获取对应的值
    console.log($.cookie("userInfo"))
});

// 登录
function login() {
    $("#loadingModal").modal("show");
    console.log("进来了")
    // 用户名
    var tel = $("#tel").val();
    var pwd = $("#pwd").val();
    if ("" == tel || "" == pwd ) {
        toastr.warning(TEL_OR_PWD_IS_NULL);
        return
    }

    // 获取公钥
    var getPublicKey = $.ajax({
        url: "../api/getPublicKey",
        type: "post",
        dataType: "json",
        data: {
            userInfo: tel
        },
        success: function (data) {

            console.log(data);
            // 业务逻辑成功
            if (0 == data.code) {
                // 提取公钥
                var publicKey = data.data;
                // 声明加密对象
                var encrypt = new JSEncrypt();
                // 设置公钥
                encrypt.setPublicKey(publicKey);
                // 对密码加密
                var encryptPwd = encrypt.encrypt(pwd);
                console.log(encryptPwd);

                // 调用登录的网络请求方法，并传递两个参数：电话、加密后的密码
                loginRequest(tel, encryptPwd)


            } else {
                // 错误提示：获取公钥失败
                toastr.error(GET_PUBLIC_KEY_FAILED)
            }
        },
        error: function (e) {
            // console.log(e)
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            $("#loadingModal").modal("hide");
            // 完成时的回调
            console.log("出来了")

        }
    })
}

$("#vimg").on("click", function () {
    var timestamp = (new Date()).valueOf();
    $(this).attr("src", "../api/imageCode/validateCode?timestamp=" + timestamp);
});

// 登录的网络请求
function loginRequest(tel, pwd) {
    var loginRequestAjax = $.ajax({
        url: "../api/manager/login",

        type: "post",
        data: {
            mob: tel,
            pwd: pwd,


        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            console.log(data);
            if (0 == data.code) {
                console.log(data.data.token);
                $.cookie("token", data.data.token);
                // 跳页
                location.href = "../babyeducation/index";
                //location.href = "index.html"
                // $(location).attr("href", "index.html")
            } else {
                console.log(data.message)
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
            if ("timeout" == status) {
                toastr.warning(REQUEST_TIMEOUT);
                // 终止网络请求
                loginRequestAjax.abort()
            }
        }
    })
}

// 文本输入框的改变事件
$("#username").change(function () {
    console.log($(this).val())

});

//  获取验证码
function getCode() {
    $('.get_code_btn').attr('disabled', 'true')
    $('.get_code_btn').text("获取验证码（60s）")
    var time = 60 * 5
    var interval = setInterval(function () {
        time--
        // console.log(time)
        $('.get_code_btn').text("获取验证码（" + time + "s）")

        if (0 === time) {
            clearInterval(interval)
            $('.get_code_btn').removeAttr('disabled')
            $('.get_code_btn').text("获取验证码")

        }
    }, 1000)

}

// 验证码网络请求
function sendMsg() {
    var tel = $(".tel").val()
    console.log(tel);
    var sendMsg = $.ajax({
        url: "../api/manager/sendMsg",
        type: "put",
        data: {
            tel: tel,
        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            console.log(data);
            if ("success" == data.flag) {
                getCode();
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
            if ("timeout" == status) {
                toastr.warning(REQUEST_TIMEOUT);
                // 终止网络请求
                sendMsg.abort()
            }
        }
    })
}

// 修改密码网络请求
function changePwd() {
    var tel = $(".tel").val()
    var code = $(".code").val()
    var pwd = $(".pwd").val()

    console.log(tel);
    var sendMsg = $.ajax({
        url: "../api/manager/changePwd",
        type: "put",
        data: {
            tel: tel,
            code: code,
            pwd: pwd,
        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            console.log(data);
            if ("success" == data.flag) {
                toastr.success(data.message)
                $('#forgetPwd').modal('hide')
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
            if ("timeout" == status) {
                toastr.warning(REQUEST_TIMEOUT);
                // 终止网络请求
                sendMsg.abort()
            }
        }
    })

}


