//提示框初始化，toast-top-center表示提示框的位置
toastr.options = {
    positionClass: 'toast-top-center', // 提示框位置
    closeButton: true  // 是否显示关闭按钮
};

$().ready(function () {
    var validateOptions = {
        rules: {
            oldPwd: {
                required: true,
                minlength: 6
            },
            newPwd: {
                required: true,
                minlength: 6
            },
            confirmPwd: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            oldPwd: {
                required: "旧密码必填",
                minlength: "旧密码长度不能小于六个字符"
            },
            newPwd: {
                required: "新密码必填",
                minlength: "新密码长度不能小于六个字符"
            },
            confirmPwd: {
                required: "确认密码必填",
                minlength: "确认密码长度不能小于六个字符"
            }
        }
    }
    // form表单验证规则配置
    $(".personal_center").validate(validateOptions)

})


// 修改信息
function updatePwd() {
    console.log("修改密码");
    // 显示loading
    $("#loadingModal").modal("show");

    // 获取详情模态框输入框中的值
    var oldPwd = $(".oldPwd").val();
    var newPwd = $(".newPwd").val();
    var confirmPwd = $(".confirmPwd").val();
    if (newPwd == confirmPwd) {
        var updatePwdjax = $.ajax({
            url: "/api/manager/changerPwd",
            type: "put",
            data: {
                oldPwd: hex_md5(oldPwd),
                confirmPwd: hex_md5(confirmPwd)
            },
            timeout: 5000,
            dataType: "json",
            //设置header请求头
            beforeSend: function (request) {
                request.setRequestHeader("x-auth-token", $.cookie("token"));
            },
            success: function (data) {
                console.log(data)
                if ("success" == data.flag) {
                    // 提示用户，操作成功
                    toastr.success(UPDATE_SUCCESS_INFO)
                } else {
                    // 业务逻辑失败提示用户
                    toastr.error(data.message)
                }
            },
            error: function (e) {
                if ("timeout" == e.statusText) {
                    // 请求超时，终止请求
                    toastr.warning(REQUEST_TIMEOUT)
                    updatePwdjax.abort()
                } else {
                    toastr.error(NETWORK_ERROR)
                }
            },
            complete: function (xhr, status) {
                // 隐藏loading
                $("#loadingModal").modal("hide")
            }
        })

    } else {
        toastr.error("两次密码不一致")
        $("#loadingModal").modal("hide")
    }

}
