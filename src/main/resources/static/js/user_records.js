//提示框初始化，toast-top-center表示提示框的位置
toastr.options = {
    positionClass: 'toast-top-center', // 提示框位置
    closeButton: true  // 是否显示关闭按钮
};
var pageNum = 1;

$(document).ready(function () {
    getGarden();
    getSubject();
    getPress();
    searchUserRecords(pageNum);
});

// 总查询
function searchUserRecords(pageNum) {
    $("#loadingModal").modal("show");
    var mob = $("#searchDiv .search_mob").val();
    var searchManagerAjax = $.ajax({
        url: "../api/userRecords/searchUserRecords",
        type: "GET",
        data: {
            mob: mob,
            pageNum: pageNum,
            pageSize: PAGE_SIZE
        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if (0 == data.code) {
                var str = '';
                var list = data.data.list;
                if (0 == list.length) {
                    // 没有数据，显示提示
                    $("#no_data").show()
                } else {
                    // 有数据，隐藏提示
                    $("#no_data").hide()
                }
                for (var i in list) {
                    var item = list[i];
                    var volume = 0 == item.volume ? "上册" : "下册";
                    // 处理性别字段
                    str += "<tr>\n" +
                        "                                        <td>" + item.id + "</td>\n" +
                        "                                        <td>" + item.nickName + "</td>\n" +
                        "                                        <td>" + item.mob + "</td>\n" +
                        "                                        <td>" + item.bookId + "</td>\n" +
                        "                                        <td>" + item.bookName + "</td>\n" +
                        "                                        <td>" + item.gardenName + "</td>\n" +
                        "                                        <td>" + item.subjectName + "</td>\n" +
                        "                                        <td>" + item.pressName + "</td>\n" +
                        "                                        <td>" + volume + "</td>\n" +
                        "                                        <td>" + item.createTime + "</td>\n" +
                        "                                        <td>\n" +
                        // "                                            <button class=\"btn btn-primary tab_btn\" onclick=\"lookUser(" + JSON.stringify(item).replace(/"/g, "&quot;") + ")\">编辑</button>\n" +
                        "                                            <button class=\"btn btn-danger tab_btn\" onclick=\"deleteAks(" + item.id + ")\">删除</button>\n" +
                        "                                        </td>\n" +
                        "                                    </tr>";
                }
                // 替换表格数据
                $("#tab_body").html(str);
                $("#loadingModal").modal("hide");

                //当前页数
                var pages = data.data.pages;
                //分页
                $("#page").bootstrapPaginator({
                    bootstrapMajorVersion: 3,   //bootstrap版本
                    currentPage: pageNum,  //当前页数
                    totalPages: pages,   //总页数
                    numberOfPages: PAGE_SIZE,    //每页大小
                    shouldShowPage: true,   //所有的按钮都展示
                    itemTexts: function (type, page, current) {
                        //设置分页控件每部分的显示文字
                        switch (type) {
                            case "first":
                                return "第一页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "最后一页";
                            case "page":
                                return page;
                            case "jump":
                                return "跳转"
                        }
                    }, onPageClicked: function (event, originalEvent, type, page) {
                        //点击事件
                        //page表示分页控件当前的页码，用这个页码重新走网络请求，会拿到对应的数据
                        searchUserRecords(page);
                    }
                })
            } else {
                toastr.error(data.message)
            }
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR)
            searchManagerAjax.abort()
        },
        complete: function (xhr, status) {
            $("#loadingModal").modal("hide");
            // 完成时的回调
        }
    })
}


function deleteAks(id) {
    confirmBySelf(DELETE_INFO, function () {
        deleteUserRecords(id)
    })
}

function deleteUserRecords(id) {
    var updateSessionAjax = $.ajax({
        url: "../api/userRecords/deleteUserRecords",
        type: "delete",
        data: {
            id: id,
        },
        timeout: 5000,
        dataType: "json",
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {

            if (0 == data.code) {
                // 修改状态成功后调用查询接口，就能刷新列表
                searchUserRecords($("#input_page").val());
                // 提示用户，操作成功
                toastr.success(DELETE_SUCCESS_INFO)
            } else {
                // 业务逻辑失败提示用户
                toastr.error(data.message)
            }
        },
        error: function (e) {
            if ("timeout" == e.statusText) {
                // 请求超时，终止请求
                toastr.warning(REQUEST_TIMEOUT)
                updateSessionAjax.abort()
            } else {
                toastr.error(NETWORK_ERROR)
            }
        },
        complete: function (xhr, status) {

        }
    });
}

//编辑前查看
function lookUser(item) {
    // 显示修改信息
    // 获取详情模态框输入框中的值
    $(".id").val(item.userId);
    $(".nick_name").val(item.nickName);
    $(".user_url").val(item.userUrl);
    $(".mob").val(item.mob);
    $(".is_vip").val(item.isVip);
    $(".stat_time").val(item.statTime);
    $(".end_time").val(item.endTime);

    $("#detail_modal").modal("show");

}

// 编辑个人信息
function updateUser() {
    // 获取详情模态框输入框中的值
    var userId = $("#detail_modal .id").val();
    var isVip = $("#detail_modal .is_vip").val();
    var statTime = $("#detail_modal .stat_time").val();
    var endTime = $("#detail_modal .end_time").val();
    var updateMPSAjax = $.ajax({
        url: "../api/user/updateUser",
        type: "put",
        data: {
            userId: userId,
            isVip: isVip,
            statTime: statTime,
            endTime: endTime
        },
        timeout: 5000,
        dataType: "json",
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if (0 == data.code) {
                // 修改状态成功后调用查询接口，就能刷新列表
                searchUser(pageNum);
                // 提示用户，操作成功
                toastr.success(UPDATE_SUCCESS_INFO)
            } else {
                // 业务逻辑失败提示用户
                toastr.error(data.message)
            }
        },
        error: function (e) {
            if ("timeout" == e.statusText) {
                // 请求超时，终止请求
                toastr.warning(REQUEST_TIMEOUT)
                updateMPSAjax.abort()
            } else {
                toastr.error(NETWORK_ERROR)
            }
        },
        complete: function (xhr, status) {

        }
    });
    $("#detail_modal").modal("hide");

}


//下拉条查询年级
function getGarden() {
    $.ajax({
        url: "../api/garden/selectGarden",
        data: {},
        type: "POST",
        success: function (data) {

            if (data != null) {
                var list = data.data.list;
                var html = "<option value=''>请选择年级</option>"
                for (var i = 0; i < list.length; i++) {
                    html += "<option value='" + list[i].id + "'>" + list[i].gardenName + "</option>";
                }
                $("#new_modal .search_garden_id").html(html);
            }
        }
    });
}

//下拉条查询年级
function getSubject() {
    $.ajax({
        url: "../api/subject/selectSubject",
        data: {},
        type: "POST",
        success: function (data) {

            if (data != null) {
                var list = data.data.list;
                var html = "<option value=''>请选择科目</option>"
                for (var i = 0; i < list.length; i++) {
                    html += "<option value='" + list[i].id + "'>" + list[i].subjectName + "</option>";
                }
                $("#new_modal .search_subject_id").html(html);
            }
        }
    });
}

//下拉条查询出版社
function getPress() {
    $.ajax({
        url: "../api/press/selectPress",
        data: {},
        type: "POST",
        success: function (data) {

            if (data != null) {
                var list = data.data.list;
                var html = "<option value=''>请选择出版社</option>"
                for (var i = 0; i < list.length; i++) {
                    html += "<option value='" + list[i].id + "'>" + list[i].pressName + "</option>";
                }
                $("#new_modal .search_press_id").html(html);
            }
        }
    });
}

function searchUserId() {
    var mob = $("#input1").val();
    $.ajax({
        url: "../api/user/selectUser",
        data: {
            mob: mob
        },
        type: "GET",
        dataType: "json",
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if (data != null) {
                var list = data.data.list;
                var userId
                if (0 != list.length) {
                    userId = list[0].userId
                } else {
                    userId = '此手机号错误没有用户ID'
                }

                $("#new_modal .user_id").val(userId);
            }
        }

    });
}

function searchBookId() {
    var gardenId = $("#new_modal .search_garden_id").val();
    var pressId = $("#new_modal .search_press_id").val();
    var subjectId = $("#new_modal .search_subject_id").val();
    var volume = $("#new_modal .volume").val();
    $.ajax({
        url: "../api/book/searchBookInfo",
        data: {
            gardenId: gardenId,
            pressId: pressId,
            subjectId: subjectId,
            volume: volume
        },
        type: "GET",
        dataType: "json",
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if (data != null) {
                var list = data.data.list;
                var bookId
                var bookName
                if (0 != list.length) {
                    bookId = list[0].bookId
                    bookName = list[0].bookName
                } else {
                    bookId = '没有书ID'
                    bookName = '没有书名'
                }

                $("#new_modal .book_id").val(bookId);
                $("#new_modal .book_name").val(bookName);
            }
        }

    });


}

function addUserRecords(){
    var userId = $("#new_modal .user_id").val();
    var   bookId =  $("#new_modal .book_id").val();
    $.ajax({
        url: "../api/userRecords/addUserRecords",
        data: {
            userId: userId,
            bookId: bookId,

        },
        type: "POST",
        dataType: "json",
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        success: function (data) {
            if (0 == data.code) {
                searchUserRecords($("#input_page").val());
            }
            toastr.success(ADD_SUCCESS_INFO)
        },
        error: function (e) {
            toastr.error(NETWORK_ERROR);

        },
        complete: function (xhr, status) {
            $("#new_modal").modal("hide");

        }


    });


}

//模态框关闭是清空内容
$('#new_modal').on('hidden.bs.modal', function () {
    document.getElementById("new_form").reset();
});
$('#detail_modal').on('hidden.bs.modal', function () {
    document.getElementById("detail_form").reset();
});

