//加载顶部菜单栏
$(".header-section").load("../babyeducation/top_menu");
// 加载左边栏
$(".left-side").load("../babyeducation/left_menu");
//设置顶部菜单栏名称
var username = 'admin';
$("#username").html(username);
//获取管理员信息
searchManagerInfo();

function searchManagerInfo() {
    $.ajax({
        url: "../api/manager/searchManagerInfo",
        type: "POST",
        data: {},
        //设置header请求头
        beforeSend: function (request) {
            request.setRequestHeader("x-auth-token", $.cookie("token"));
        },
        dataType: "json",
        timeout: 5000,  // 超时时间 单位：毫秒
        success: function (data) {
            if (0 == data.code) {
                username = data.data.name;
                $("#username").html(username)
            } else {
                //连接失败
                //toastr.warning(data.message);
                alert(data.message);
                // 跳页
                location.href = "../babyeducation/login"
            }
        },
        error: function (e) {
            //连接失败
            toastr.error(NETWORK_ERROR)
        },
        complete: function (xhr, status) {
            // 完成时的回调
            if ("timeout" == status) {
                //连接超时
                toastr.warning(REQUEST_TIMEOUT)
            }
        }
    });

}

// 时间戳转换
function timestampToDate(timestamp, flag, dateSymbol) {
    // console.log(timestamp)
    // console.log(flag)
    // console.log(dateSymbol)
    var date = new Date()
    date.setTime(timestamp)
    // console.log(time)
    var year = date.getFullYear()
    // console.log(year)
    var month = date.getMonth() + 1
    month = month < 10 ? ("0" + month) : month
    // console.log(month)
    var day = date.getDate()
    // console.log(date)
    var hours = date.getHours()
    hours = hours < 10 ? ("0" + hours) : hours
    // console.log(hours)
    var minutes = date.getMinutes()
    minutes = minutes < 10 ? ("0" + minutes) : minutes
    // console.log(minutes)
    var seconds = date.getSeconds()
    seconds = seconds < 10 ? ("0" + seconds) : seconds
    // console.log(seconds)
    // console.log(date)
    if (flag) {
        return year + dateSymbol + month + dateSymbol + day + " " + hours + ":" + minutes + ":" + seconds
    } else {
        return year + dateSymbol + month + dateSymbol + day
    }
}

/**
 *对话框
 * @param content  提示内容
 * @param func      点击对话框确认按钮做什么，类型为function
 */
function confirmBySelf(content, func) {
    $.confirm({
        confirmButtonClass: "btn-primary",   //设置ok按钮样式
        closeIcon: true,        //对话框右上角关闭
        confirmButton: "确认", //确认按钮标题
        cancelButton: "取消", //取消按钮标题
        title: "提示",        //标题
        content: content,        //对话内容
        confirm: function () {
            //点击确认后做什么
            func()
        },
        cancel: function () {//点击取消后做什么

        }
    })
}

// 用户身份信息失效时的操作
function userInfoInvalid() {
    // 身份信息失效，强制重新登录
    confirmBySelf(IDENTITY_FAILURE, function () {
        location.href = "../babyeducation/login"
    })

}


